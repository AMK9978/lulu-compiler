package LuluCompiler;// Generated from /home/amk/IdeaProjects/LULU/src/lulu.g4 by ANTLR 4.7.2

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class luluParser extends Parser {
    static {
        RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION);
    }

    protected static final DFA[] _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache =
            new PredictionContextCache();
    public static final int
            T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, T__7 = 8, T__8 = 9,
            T__9 = 10, T__10 = 11, T__11 = 12, T__12 = 13, T__13 = 14, T__14 = 15, T__15 = 16, T__16 = 17,
            T__17 = 18, T__18 = 19, T__19 = 20, T__20 = 21, T__21 = 22, T__22 = 23, T__23 = 24,
            T__24 = 25, T__25 = 26, T__26 = 27, T__27 = 28, Ws = 29, Comment = 30, Int = 31, Float = 32,
            Bool = 33, String = 34, Function = 35, If = 36, Else = 37, Switch = 38, Default = 39,
            Break = 40, For = 41, While = 42, Continue = 43, Const = 44, Caseof = 45, This = 46,
            Super = 47, Type = 48, Declare = 49, Read = 50, Write = 51, Nil = 52, Destruct = 53,
            Allocate = 54, Public = 55, Private = 56, Protected = 57, Int_val = 58, Float_val = 59,
            String_val = 60, Bool_val = 61, ID = 62;
    public static final int
            RULE_program = 0, RULE_test = 1, RULE_ft_dcl = 2, RULE_func_dcl = 3, RULE_args = 4,
            RULE_args_var = 5, RULE_type_dcl = 6, RULE_var_def = 7, RULE_var_val = 8,
            RULE_ft_def = 9, RULE_type_def = 10, RULE_component = 11, RULE_access_modifier = 12,
            RULE_fun_def = 13, RULE_block = 14, RULE_stmt = 15, RULE_assign = 16,
            RULE_var = 17, RULE_ref = 18, RULE_expr = 19, RULE_func_call = 20, RULE_list = 21,
            RULE_handle_call = 22, RULE_params = 23, RULE_cond_stmt = 24, RULE_switch_body = 25,
            RULE_loop_stmt = 26, RULE_type = 27, RULE_const_val = 28, RULE_unary_op = 29,
            RULE_op1 = 30, RULE_op2 = 31, RULE_op3 = 32, RULE_op4 = 33, RULE_bitwise = 34,
            RULE_logical = 35;

    private static String[] makeRuleNames() {
        return new String[]{
                "program", "test", "ft_dcl", "func_dcl", "args", "args_var", "type_dcl",
                "var_def", "var_val", "ft_def", "type_def", "component", "access_modifier",
                "fun_def", "block", "stmt", "assign", "var", "ref", "expr", "func_call",
                "list", "handle_call", "params", "cond_stmt", "switch_body", "loop_stmt",
                "type", "const_val", "unary_op", "op1", "op2", "op3", "op4", "bitwise",
                "logical"
        };
    }

    public static final String[] ruleNames = makeRuleNames();

    private static String[] makeLiteralNames() {
        return new String[]{
                null, "'{'", "'}'", "'('", "')'", "'='", "';'", "'['", "']'", "','",
                "':'", "'.'", "'!'", "'~'", "'-'", "'*'", "'/'", "'%'", "'+'", "'<='",
                "'>='", "'<'", "'>'", "'=='", "'!='", "'&'", "'|'", "'||'", "'&&'", null,
                null, "'int'", "'float'", "'bool'", "'string'", "'function'", "'if'",
                "'else'", "'switch'", "'default'", "'break'", "'for'", "'while'", "'continue'",
                "'const'", "'caseof'", "'this'", "'super'", "'type'", "'declare'", "'read'",
                "'write'", "'nil'", "'destruct'", "'allocate'", "'public'", "'private'",
                "'protected'"
        };
    }

    private static final String[] _LITERAL_NAMES = makeLiteralNames();

    private static String[] makeSymbolicNames() {
        return new String[]{
                null, null, null, null, null, null, null, null, null, null, null, null,
                null, null, null, null, null, null, null, null, null, null, null, null,
                null, null, null, null, null, "Ws", "Comment", "Int", "Float", "Bool",
                "String", "Function", "If", "Else", "Switch", "Default", "Break", "For",
                "While", "Continue", "Const", "Caseof", "This", "Super", "Type", "Declare",
                "Read", "Write", "Nil", "Destruct", "Allocate", "Public", "Private",
                "Protected", "Int_val", "Float_val", "String_val", "Bool_val", "ID"
        };
    }

    private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
    public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

    /**
     * @deprecated Use {@link #VOCABULARY} instead.
     */
    @Deprecated
    public static final String[] tokenNames;

    static {
        tokenNames = new String[_SYMBOLIC_NAMES.length];
        for (int i = 0; i < tokenNames.length; i++) {
            tokenNames[i] = VOCABULARY.getLiteralName(i);
            if (tokenNames[i] == null) {
                tokenNames[i] = VOCABULARY.getSymbolicName(i);
            }

            if (tokenNames[i] == null) {
                tokenNames[i] = "<INVALID>";
            }
        }
    }

    @Override
    @Deprecated
    public String[] getTokenNames() {
        return tokenNames;
    }

    @Override

    public Vocabulary getVocabulary() {
        return VOCABULARY;
    }

    @Override
    public String getGrammarFileName() {
        return "lulu.g4";
    }

    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }

    @Override
    public String getSerializedATN() {
        return _serializedATN;
    }

    @Override
    public ATN getATN() {
        return _ATN;
    }

    public luluParser(TokenStream input) {
        super(input);
        _interp = new ParserATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }

    public static class ProgramContext extends ParserRuleContext {
        public TerminalNode EOF() {
            return getToken(luluParser.EOF, 0);
        }

        public Ft_dclContext ft_dcl() {
            return getRuleContext(Ft_dclContext.class, 0);
        }

        public List<Ft_defContext> ft_def() {
            return getRuleContexts(Ft_defContext.class);
        }

        public Ft_defContext ft_def(int i) {
            return getRuleContext(Ft_defContext.class, i);
        }

        public ProgramContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_program;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterProgram(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitProgram(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitProgram(this);
            else return visitor.visitChildren(this);
        }
    }

    public final ProgramContext program() throws RecognitionException {
        ProgramContext _localctx = new ProgramContext(_ctx, getState());
        enterRule(_localctx, 0, RULE_program);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(73);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == Declare) {
                    {
                        setState(72);
                        ft_dcl();
                    }
                }

                setState(76);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(75);
                            ft_def();
                        }
                    }
                    setState(78);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << Function) | (1L << Type))) != 0));
                setState(80);
                match(EOF);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class TestContext extends ParserRuleContext {
        public ExprContext expr() {
            return getRuleContext(ExprContext.class, 0);
        }

        public TerminalNode EOF() {
            return getToken(luluParser.EOF, 0);
        }

        public TestContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_test;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterTest(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitTest(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitTest(this);
            else return visitor.visitChildren(this);
        }
    }

    public final TestContext test() throws RecognitionException {
        TestContext _localctx = new TestContext(_ctx, getState());
        enterRule(_localctx, 2, RULE_test);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(82);
                expr(0);
                setState(83);
                match(EOF);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Ft_dclContext extends ParserRuleContext {
        public TerminalNode Declare() {
            return getToken(luluParser.Declare, 0);
        }

        public List<Func_dclContext> func_dcl() {
            return getRuleContexts(Func_dclContext.class);
        }

        public Func_dclContext func_dcl(int i) {
            return getRuleContext(Func_dclContext.class, i);
        }

        public List<Type_dclContext> type_dcl() {
            return getRuleContexts(Type_dclContext.class);
        }

        public Type_dclContext type_dcl(int i) {
            return getRuleContext(Type_dclContext.class, i);
        }

        public List<Var_defContext> var_def() {
            return getRuleContexts(Var_defContext.class);
        }

        public Var_defContext var_def(int i) {
            return getRuleContext(Var_defContext.class, i);
        }

        public Ft_dclContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_ft_dcl;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterFt_dcl(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitFt_dcl(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitFt_dcl(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Ft_dclContext ft_dcl() throws RecognitionException {
        Ft_dclContext _localctx = new Ft_dclContext(_ctx, getState());
        enterRule(_localctx, 4, RULE_ft_dcl);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(85);
                match(Declare);
                setState(86);
                match(T__0);
                setState(90);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        setState(90);
                        _errHandler.sync(this);
                        switch (getInterpreter().adaptivePredict(_input, 2, _ctx)) {
                            case 1: {
                                setState(87);
                                func_dcl();
                            }
                            break;
                            case 2: {
                                setState(88);
                                type_dcl();
                            }
                            break;
                            case 3: {
                                setState(89);
                                var_def();
                            }
                            break;
                        }
                    }
                    setState(92);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << Int) | (1L << Float) | (1L << Bool) | (1L << String) | (1L << Const) | (1L << ID))) != 0));
                setState(94);
                match(T__1);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Func_dclContext extends ParserRuleContext {
        public TerminalNode ID() {
            return getToken(luluParser.ID, 0);
        }

        public List<ArgsContext> args() {
            return getRuleContexts(ArgsContext.class);
        }

        public ArgsContext args(int i) {
            return getRuleContext(ArgsContext.class, i);
        }

        public Args_varContext args_var() {
            return getRuleContext(Args_varContext.class, 0);
        }

        public Func_dclContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_func_dcl;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterFunc_dcl(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitFunc_dcl(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitFunc_dcl(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Func_dclContext func_dcl() throws RecognitionException {
        Func_dclContext _localctx = new Func_dclContext(_ctx, getState());
        enterRule(_localctx, 6, RULE_func_dcl);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(101);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__2) {
                    {
                        setState(96);
                        match(T__2);
                        setState(97);
                        args(0);
                        setState(98);
                        match(T__3);
                        setState(99);
                        match(T__4);
                    }
                }

                setState(103);
                match(ID);
                setState(104);
                match(T__2);
                setState(107);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 5, _ctx)) {
                    case 1: {
                        setState(105);
                        args(0);
                    }
                    break;
                    case 2: {
                        setState(106);
                        args_var(0);
                    }
                    break;
                }
                setState(109);
                match(T__3);
                setState(110);
                match(T__5);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ArgsContext extends ParserRuleContext {
        public TypeContext type() {
            return getRuleContext(TypeContext.class, 0);
        }

        public ArgsContext args() {
            return getRuleContext(ArgsContext.class, 0);
        }

        public ArgsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_args;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterArgs(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitArgs(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitArgs(this);
            else return visitor.visitChildren(this);
        }
    }

    public final ArgsContext args() throws RecognitionException {
        return args(0);
    }

    private ArgsContext args(int _p) throws RecognitionException {
        ParserRuleContext _parentctx = _ctx;
        int _parentState = getState();
        ArgsContext _localctx = new ArgsContext(_ctx, _parentState);
        ArgsContext _prevctx = _localctx;
        int _startState = 8;
        enterRecursionRule(_localctx, 8, RULE_args, _p);
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                {
                    setState(113);
                    type();
                    setState(118);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 6, _ctx);
                    while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                        if (_alt == 1) {
                            {
                                {
                                    setState(114);
                                    match(T__6);
                                    setState(115);
                                    match(T__7);
                                }
                            }
                        }
                        setState(120);
                        _errHandler.sync(this);
                        _alt = getInterpreter().adaptivePredict(_input, 6, _ctx);
                    }
                }
                _ctx.stop = _input.LT(-1);
                setState(133);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 8, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        if (_parseListeners != null) triggerExitRuleEvent();
                        _prevctx = _localctx;
                        {
                            {
                                _localctx = new ArgsContext(_parentctx, _parentState);
                                pushNewRecursionContext(_localctx, _startState, RULE_args);
                                setState(121);
                                if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
                                setState(122);
                                match(T__8);
                                setState(123);
                                type();
                                setState(128);
                                _errHandler.sync(this);
                                _alt = getInterpreter().adaptivePredict(_input, 7, _ctx);
                                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                                    if (_alt == 1) {
                                        {
                                            {
                                                setState(124);
                                                match(T__6);
                                                setState(125);
                                                match(T__7);
                                            }
                                        }
                                    }
                                    setState(130);
                                    _errHandler.sync(this);
                                    _alt = getInterpreter().adaptivePredict(_input, 7, _ctx);
                                }
                            }
                        }
                    }
                    setState(135);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 8, _ctx);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            unrollRecursionContexts(_parentctx);
        }
        return _localctx;
    }

    public static class Args_varContext extends ParserRuleContext {
        public TypeContext type() {
            return getRuleContext(TypeContext.class, 0);
        }

        public TerminalNode ID() {
            return getToken(luluParser.ID, 0);
        }

        public Args_varContext args_var() {
            return getRuleContext(Args_varContext.class, 0);
        }

        public Args_varContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_args_var;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterArgs_var(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitArgs_var(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitArgs_var(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Args_varContext args_var() throws RecognitionException {
        return args_var(0);
    }

    private Args_varContext args_var(int _p) throws RecognitionException {
        ParserRuleContext _parentctx = _ctx;
        int _parentState = getState();
        Args_varContext _localctx = new Args_varContext(_ctx, _parentState);
        Args_varContext _prevctx = _localctx;
        int _startState = 10;
        enterRecursionRule(_localctx, 10, RULE_args_var, _p);
        int _la;
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                {
                    setState(137);
                    type();
                    setState(142);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    while (_la == T__6) {
                        {
                            {
                                setState(138);
                                match(T__6);
                                setState(139);
                                match(T__7);
                            }
                        }
                        setState(144);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                    }
                    setState(145);
                    match(ID);
                }
                _ctx.stop = _input.LT(-1);
                setState(161);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 11, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        if (_parseListeners != null) triggerExitRuleEvent();
                        _prevctx = _localctx;
                        {
                            {
                                _localctx = new Args_varContext(_parentctx, _parentState);
                                pushNewRecursionContext(_localctx, _startState, RULE_args_var);
                                setState(147);
                                if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
                                setState(148);
                                match(T__8);
                                setState(149);
                                type();
                                setState(154);
                                _errHandler.sync(this);
                                _la = _input.LA(1);
                                while (_la == T__6) {
                                    {
                                        {
                                            setState(150);
                                            match(T__6);
                                            setState(151);
                                            match(T__7);
                                        }
                                    }
                                    setState(156);
                                    _errHandler.sync(this);
                                    _la = _input.LA(1);
                                }
                                setState(157);
                                match(ID);
                            }
                        }
                    }
                    setState(163);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 11, _ctx);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            unrollRecursionContexts(_parentctx);
        }
        return _localctx;
    }

    public static class Type_dclContext extends ParserRuleContext {
        public TerminalNode ID() {
            return getToken(luluParser.ID, 0);
        }

        public Type_dclContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_type_dcl;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterType_dcl(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitType_dcl(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitType_dcl(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Type_dclContext type_dcl() throws RecognitionException {
        Type_dclContext _localctx = new Type_dclContext(_ctx, getState());
        enterRule(_localctx, 12, RULE_type_dcl);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(164);
                match(ID);
                setState(165);
                match(T__5);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Var_defContext extends ParserRuleContext {
        public TypeContext type() {
            return getRuleContext(TypeContext.class, 0);
        }

        public List<Var_valContext> var_val() {
            return getRuleContexts(Var_valContext.class);
        }

        public Var_valContext var_val(int i) {
            return getRuleContext(Var_valContext.class, i);
        }

        public TerminalNode Const() {
            return getToken(luluParser.Const, 0);
        }

        public Var_defContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_var_def;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterVar_def(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitVar_def(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitVar_def(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Var_defContext var_def() throws RecognitionException {
        Var_defContext _localctx = new Var_defContext(_ctx, getState());
        enterRule(_localctx, 14, RULE_var_def);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(168);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == Const) {
                    {
                        setState(167);
                        match(Const);
                    }
                }

                setState(170);
                type();
                setState(171);
                var_val();
                setState(176);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == T__8) {
                    {
                        {
                            setState(172);
                            match(T__8);
                            setState(173);
                            var_val();
                        }
                    }
                    setState(178);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(179);
                match(T__5);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Var_valContext extends ParserRuleContext {
        public RefContext ref() {
            return getRuleContext(RefContext.class, 0);
        }

        public ExprContext expr() {
            return getRuleContext(ExprContext.class, 0);
        }

        public Var_valContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_var_val;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterVar_val(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitVar_val(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitVar_val(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Var_valContext var_val() throws RecognitionException {
        Var_valContext _localctx = new Var_valContext(_ctx, getState());
        enterRule(_localctx, 16, RULE_var_val);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(181);
                ref();
                setState(184);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__4) {
                    {
                        setState(182);
                        match(T__4);
                        setState(183);
                        expr(0);
                    }
                }

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Ft_defContext extends ParserRuleContext {
        public Type_defContext type_def() {
            return getRuleContext(Type_defContext.class, 0);
        }

        public Fun_defContext fun_def() {
            return getRuleContext(Fun_defContext.class, 0);
        }

        public Ft_defContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_ft_def;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterFt_def(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitFt_def(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitFt_def(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Ft_defContext ft_def() throws RecognitionException {
        Ft_defContext _localctx = new Ft_defContext(_ctx, getState());
        enterRule(_localctx, 18, RULE_ft_def);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(188);
                _errHandler.sync(this);
                switch (_input.LA(1)) {
                    case Type: {
                        setState(186);
                        type_def();
                    }
                    break;
                    case T__2:
                    case Function: {
                        setState(187);
                        fun_def();
                    }
                    break;
                    default:
                        throw new NoViableAltException(this);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Type_defContext extends ParserRuleContext {
        public TerminalNode Type() {
            return getToken(luluParser.Type, 0);
        }

        public List<TerminalNode> ID() {
            return getTokens(luluParser.ID);
        }

        public TerminalNode ID(int i) {
            return getToken(luluParser.ID, i);
        }

        public List<ComponentContext> component() {
            return getRuleContexts(ComponentContext.class);
        }

        public ComponentContext component(int i) {
            return getRuleContext(ComponentContext.class, i);
        }

        public Type_defContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_type_def;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterType_def(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitType_def(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitType_def(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Type_defContext type_def() throws RecognitionException {
        Type_defContext _localctx = new Type_defContext(_ctx, getState());
        enterRule(_localctx, 20, RULE_type_def);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(190);
                match(Type);
                setState(191);
                match(ID);
                setState(194);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__9) {
                    {
                        setState(192);
                        match(T__9);
                        setState(193);
                        match(ID);
                    }
                }

                setState(196);
                match(T__0);
                setState(198);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(197);
                            component();
                        }
                    }
                    setState(200);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << Int) | (1L << Float) | (1L << Bool) | (1L << String) | (1L << Function) | (1L << Const) | (1L << Public) | (1L << Private) | (1L << Protected) | (1L << ID))) != 0));
                setState(202);
                match(T__1);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ComponentContext extends ParserRuleContext {
        public Var_defContext var_def() {
            return getRuleContext(Var_defContext.class, 0);
        }

        public Fun_defContext fun_def() {
            return getRuleContext(Fun_defContext.class, 0);
        }

        public Access_modifierContext access_modifier() {
            return getRuleContext(Access_modifierContext.class, 0);
        }

        public ComponentContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_component;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterComponent(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitComponent(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitComponent(this);
            else return visitor.visitChildren(this);
        }
    }

    public final ComponentContext component() throws RecognitionException {
        ComponentContext _localctx = new ComponentContext(_ctx, getState());
        enterRule(_localctx, 22, RULE_component);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(205);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Public) | (1L << Private) | (1L << Protected))) != 0)) {
                    {
                        setState(204);
                        access_modifier();
                    }
                }

                setState(209);
                _errHandler.sync(this);
                switch (_input.LA(1)) {
                    case Int:
                    case Float:
                    case Bool:
                    case String:
                    case Const:
                    case ID: {
                        setState(207);
                        var_def();
                    }
                    break;
                    case T__2:
                    case Function: {
                        setState(208);
                        fun_def();
                    }
                    break;
                    default:
                        throw new NoViableAltException(this);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Access_modifierContext extends ParserRuleContext {
        public TerminalNode Private() {
            return getToken(luluParser.Private, 0);
        }

        public TerminalNode Public() {
            return getToken(luluParser.Public, 0);
        }

        public TerminalNode Protected() {
            return getToken(luluParser.Protected, 0);
        }

        public Access_modifierContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_access_modifier;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterAccess_modifier(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitAccess_modifier(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitAccess_modifier(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Access_modifierContext access_modifier() throws RecognitionException {
        Access_modifierContext _localctx = new Access_modifierContext(_ctx, getState());
        enterRule(_localctx, 24, RULE_access_modifier);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(211);
                _la = _input.LA(1);
                if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Public) | (1L << Private) | (1L << Protected))) != 0))) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Fun_defContext extends ParserRuleContext {
        public TerminalNode Function() {
            return getToken(luluParser.Function, 0);
        }

        public TerminalNode ID() {
            return getToken(luluParser.ID, 0);
        }

        public BlockContext block() {
            return getRuleContext(BlockContext.class, 0);
        }

        public List<Args_varContext> args_var() {
            return getRuleContexts(Args_varContext.class);
        }

        public Args_varContext args_var(int i) {
            return getRuleContext(Args_varContext.class, i);
        }

        public Fun_defContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_fun_def;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterFun_def(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitFun_def(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitFun_def(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Fun_defContext fun_def() throws RecognitionException {
        Fun_defContext _localctx = new Fun_defContext(_ctx, getState());
        enterRule(_localctx, 26, RULE_fun_def);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(218);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__2) {
                    {
                        setState(213);
                        match(T__2);
                        setState(214);
                        args_var(0);
                        setState(215);
                        match(T__3);
                        setState(216);
                        match(T__4);
                    }
                }

                setState(220);
                match(Function);
                setState(221);
                match(ID);
                setState(222);
                match(T__2);
                setState(224);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Int) | (1L << Float) | (1L << Bool) | (1L << String) | (1L << ID))) != 0)) {
                    {
                        setState(223);
                        args_var(0);
                    }
                }

                setState(226);
                match(T__3);
                setState(227);
                block();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class BlockContext extends ParserRuleContext {
        public List<Var_defContext> var_def() {
            return getRuleContexts(Var_defContext.class);
        }

        public Var_defContext var_def(int i) {
            return getRuleContext(Var_defContext.class, i);
        }

        public List<StmtContext> stmt() {
            return getRuleContexts(StmtContext.class);
        }

        public StmtContext stmt(int i) {
            return getRuleContext(StmtContext.class, i);
        }

        public BlockContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_block;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterBlock(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitBlock(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitBlock(this);
            else return visitor.visitChildren(this);
        }
    }

    public final BlockContext block() throws RecognitionException {
        BlockContext _localctx = new BlockContext(_ctx, getState());
        enterRule(_localctx, 28, RULE_block);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(229);
                match(T__0);
                setState(234);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << Int) | (1L << Float) | (1L << Bool) | (1L << String) | (1L << If) | (1L << Switch) | (1L << Break) | (1L << For) | (1L << While) | (1L << Continue) | (1L << Const) | (1L << This) | (1L << Super) | (1L << Read) | (1L << Write) | (1L << Destruct) | (1L << ID))) != 0)) {
                    {
                        setState(232);
                        _errHandler.sync(this);
                        switch (getInterpreter().adaptivePredict(_input, 22, _ctx)) {
                            case 1: {
                                setState(230);
                                var_def();
                            }
                            break;
                            case 2: {
                                setState(231);
                                stmt();
                            }
                            break;
                        }
                    }
                    setState(236);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(237);
                match(T__1);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class StmtContext extends ParserRuleContext {
        public AssignContext assign() {
            return getRuleContext(AssignContext.class, 0);
        }

        public Func_callContext func_call() {
            return getRuleContext(Func_callContext.class, 0);
        }

        public Cond_stmtContext cond_stmt() {
            return getRuleContext(Cond_stmtContext.class, 0);
        }

        public Loop_stmtContext loop_stmt() {
            return getRuleContext(Loop_stmtContext.class, 0);
        }

        public TerminalNode Break() {
            return getToken(luluParser.Break, 0);
        }

        public TerminalNode Continue() {
            return getToken(luluParser.Continue, 0);
        }

        public TerminalNode Destruct() {
            return getToken(luluParser.Destruct, 0);
        }

        public TerminalNode ID() {
            return getToken(luluParser.ID, 0);
        }

        public StmtContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_stmt;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterStmt(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitStmt(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitStmt(this);
            else return visitor.visitChildren(this);
        }
    }

    public final StmtContext stmt() throws RecognitionException {
        StmtContext _localctx = new StmtContext(_ctx, getState());
        enterRule(_localctx, 30, RULE_stmt);
        int _la;
        try {
            setState(261);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 25, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(239);
                    assign();
                    setState(240);
                    match(T__5);
                }
                break;
                case 2:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(242);
                    func_call();
                    setState(243);
                    match(T__5);
                }
                break;
                case 3:
                    enterOuterAlt(_localctx, 3);
                {
                    setState(245);
                    cond_stmt();
                }
                break;
                case 4:
                    enterOuterAlt(_localctx, 4);
                {
                    setState(246);
                    loop_stmt();
                }
                break;
                case 5:
                    enterOuterAlt(_localctx, 5);
                {
                    setState(247);
                    match(Break);
                    setState(248);
                    match(T__5);
                }
                break;
                case 6:
                    enterOuterAlt(_localctx, 6);
                {
                    setState(249);
                    match(Continue);
                    setState(250);
                    match(T__5);
                }
                break;
                case 7:
                    enterOuterAlt(_localctx, 7);
                {
                    setState(251);
                    match(Destruct);
                    setState(256);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    while (_la == T__6) {
                        {
                            {
                                setState(252);
                                match(T__6);
                                setState(253);
                                match(T__7);
                            }
                        }
                        setState(258);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                    }
                    setState(259);
                    match(ID);
                    setState(260);
                    match(T__5);
                }
                break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class AssignContext extends ParserRuleContext {
        public ExprContext expr() {
            return getRuleContext(ExprContext.class, 0);
        }

        public List<VarContext> var() {
            return getRuleContexts(VarContext.class);
        }

        public VarContext var(int i) {
            return getRuleContext(VarContext.class, i);
        }

        public AssignContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_assign;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterAssign(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitAssign(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitAssign(this);
            else return visitor.visitChildren(this);
        }
    }

    public final AssignContext assign() throws RecognitionException {
        AssignContext _localctx = new AssignContext(_ctx, getState());
        enterRule(_localctx, 32, RULE_assign);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(275);
                _errHandler.sync(this);
                switch (_input.LA(1)) {
                    case This:
                    case Super:
                    case ID: {
                        setState(263);
                        var();
                    }
                    break;
                    case T__2: {
                        setState(264);
                        match(T__2);
                        setState(265);
                        var();
                        setState(270);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                        while (_la == T__8) {
                            {
                                {
                                    setState(266);
                                    match(T__8);
                                    setState(267);
                                    var();
                                }
                            }
                            setState(272);
                            _errHandler.sync(this);
                            _la = _input.LA(1);
                        }
                        setState(273);
                        match(T__3);
                    }
                    break;
                    default:
                        throw new NoViableAltException(this);
                }
                setState(277);
                match(T__4);
                setState(278);
                expr(0);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class VarContext extends ParserRuleContext {
        public List<RefContext> ref() {
            return getRuleContexts(RefContext.class);
        }

        public RefContext ref(int i) {
            return getRuleContext(RefContext.class, i);
        }

        public TerminalNode This() {
            return getToken(luluParser.This, 0);
        }

        public TerminalNode Super() {
            return getToken(luluParser.Super, 0);
        }

        public VarContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_var;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterVar(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitVar(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitVar(this);
            else return visitor.visitChildren(this);
        }
    }

    public final VarContext var() throws RecognitionException {
        VarContext _localctx = new VarContext(_ctx, getState());
        enterRule(_localctx, 34, RULE_var);
        int _la;
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(282);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == This || _la == Super) {
                    {
                        setState(280);
                        _la = _input.LA(1);
                        if (!(_la == This || _la == Super)) {
                            _errHandler.recoverInline(this);
                        } else {
                            if (_input.LA(1) == Token.EOF) matchedEOF = true;
                            _errHandler.reportMatch(this);
                            consume();
                        }
                        setState(281);
                        match(T__10);
                    }
                }

                setState(284);
                ref();
                setState(289);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 29, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        {
                            {
                                setState(285);
                                match(T__10);
                                setState(286);
                                ref();
                            }
                        }
                    }
                    setState(291);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 29, _ctx);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class RefContext extends ParserRuleContext {
        public TerminalNode ID() {
            return getToken(luluParser.ID, 0);
        }

        public List<ExprContext> expr() {
            return getRuleContexts(ExprContext.class);
        }

        public ExprContext expr(int i) {
            return getRuleContext(ExprContext.class, i);
        }

        public RefContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_ref;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterRef(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitRef(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitRef(this);
            else return visitor.visitChildren(this);
        }
    }

    public final RefContext ref() throws RecognitionException {
        RefContext _localctx = new RefContext(_ctx, getState());
        enterRule(_localctx, 36, RULE_ref);
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(292);
                match(ID);
                setState(299);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 30, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        {
                            {
                                setState(293);
                                match(T__6);
                                setState(294);
                                expr(0);
                                setState(295);
                                match(T__7);
                            }
                        }
                    }
                    setState(301);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 30, _ctx);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ExprContext extends ParserRuleContext {
        public Unary_opContext unary_op() {
            return getRuleContext(Unary_opContext.class, 0);
        }

        public List<ExprContext> expr() {
            return getRuleContexts(ExprContext.class);
        }

        public ExprContext expr(int i) {
            return getRuleContext(ExprContext.class, i);
        }

        public Const_valContext const_val() {
            return getRuleContext(Const_valContext.class, 0);
        }

        public TerminalNode Allocate() {
            return getToken(luluParser.Allocate, 0);
        }

        public Handle_callContext handle_call() {
            return getRuleContext(Handle_callContext.class, 0);
        }

        public Func_callContext func_call() {
            return getRuleContext(Func_callContext.class, 0);
        }

        public VarContext var() {
            return getRuleContext(VarContext.class, 0);
        }

        public ListContext list() {
            return getRuleContext(ListContext.class, 0);
        }

        public TerminalNode Nil() {
            return getToken(luluParser.Nil, 0);
        }

        public Op1Context op1() {
            return getRuleContext(Op1Context.class, 0);
        }

        public Op2Context op2() {
            return getRuleContext(Op2Context.class, 0);
        }

        public Op3Context op3() {
            return getRuleContext(Op3Context.class, 0);
        }

        public Op4Context op4() {
            return getRuleContext(Op4Context.class, 0);
        }

        public BitwiseContext bitwise() {
            return getRuleContext(BitwiseContext.class, 0);
        }

        public LogicalContext logical() {
            return getRuleContext(LogicalContext.class, 0);
        }

        public ExprContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_expr;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterExpr(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitExpr(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitExpr(this);
            else return visitor.visitChildren(this);
        }
    }

    public final ExprContext expr() throws RecognitionException {
        return expr(0);
    }

    private ExprContext expr(int _p) throws RecognitionException {
        ParserRuleContext _parentctx = _ctx;
        int _parentState = getState();
        ExprContext _localctx = new ExprContext(_ctx, _parentState);
        ExprContext _prevctx = _localctx;
        int _startState = 38;
        enterRecursionRule(_localctx, 38, RULE_expr, _p);
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(317);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 31, _ctx)) {
                    case 1: {
                        setState(303);
                        unary_op();
                        setState(304);
                        expr(14);
                    }
                    break;
                    case 2: {
                        setState(306);
                        match(T__2);
                        setState(307);
                        expr(0);
                        setState(308);
                        match(T__3);
                    }
                    break;
                    case 3: {
                        setState(310);
                        const_val();
                    }
                    break;
                    case 4: {
                        setState(311);
                        match(Allocate);
                        setState(312);
                        handle_call();
                    }
                    break;
                    case 5: {
                        setState(313);
                        func_call();
                    }
                    break;
                    case 6: {
                        setState(314);
                        var();
                    }
                    break;
                    case 7: {
                        setState(315);
                        list();
                    }
                    break;
                    case 8: {
                        setState(316);
                        match(Nil);
                    }
                    break;
                }
                _ctx.stop = _input.LT(-1);
                setState(345);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 33, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        if (_parseListeners != null) triggerExitRuleEvent();
                        _prevctx = _localctx;
                        {
                            setState(343);
                            _errHandler.sync(this);
                            switch (getInterpreter().adaptivePredict(_input, 32, _ctx)) {
                                case 1: {
                                    _localctx = new ExprContext(_parentctx, _parentState);
                                    pushNewRecursionContext(_localctx, _startState, RULE_expr);
                                    setState(319);
                                    if (!(precpred(_ctx, 13)))
                                        throw new FailedPredicateException(this, "precpred(_ctx, 13)");
                                    setState(320);
                                    op1();
                                    setState(321);
                                    expr(14);
                                }
                                break;
                                case 2: {
                                    _localctx = new ExprContext(_parentctx, _parentState);
                                    pushNewRecursionContext(_localctx, _startState, RULE_expr);
                                    setState(323);
                                    if (!(precpred(_ctx, 12)))
                                        throw new FailedPredicateException(this, "precpred(_ctx, 12)");
                                    setState(324);
                                    op2();
                                    setState(325);
                                    expr(13);
                                }
                                break;
                                case 3: {
                                    _localctx = new ExprContext(_parentctx, _parentState);
                                    pushNewRecursionContext(_localctx, _startState, RULE_expr);
                                    setState(327);
                                    if (!(precpred(_ctx, 11)))
                                        throw new FailedPredicateException(this, "precpred(_ctx, 11)");
                                    setState(328);
                                    op3();
                                    setState(329);
                                    expr(12);
                                }
                                break;
                                case 4: {
                                    _localctx = new ExprContext(_parentctx, _parentState);
                                    pushNewRecursionContext(_localctx, _startState, RULE_expr);
                                    setState(331);
                                    if (!(precpred(_ctx, 10)))
                                        throw new FailedPredicateException(this, "precpred(_ctx, 10)");
                                    setState(332);
                                    op4();
                                    setState(333);
                                    expr(11);
                                }
                                break;
                                case 5: {
                                    _localctx = new ExprContext(_parentctx, _parentState);
                                    pushNewRecursionContext(_localctx, _startState, RULE_expr);
                                    setState(335);
                                    if (!(precpred(_ctx, 9)))
                                        throw new FailedPredicateException(this, "precpred(_ctx, 9)");
                                    setState(336);
                                    bitwise();
                                    setState(337);
                                    expr(10);
                                }
                                break;
                                case 6: {
                                    _localctx = new ExprContext(_parentctx, _parentState);
                                    pushNewRecursionContext(_localctx, _startState, RULE_expr);
                                    setState(339);
                                    if (!(precpred(_ctx, 8)))
                                        throw new FailedPredicateException(this, "precpred(_ctx, 8)");
                                    setState(340);
                                    logical();
                                    setState(341);
                                    expr(9);
                                }
                                break;
                            }
                        }
                    }
                    setState(347);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 33, _ctx);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            unrollRecursionContexts(_parentctx);
        }
        return _localctx;
    }

    public static class Func_callContext extends ParserRuleContext {
        public Handle_callContext handle_call() {
            return getRuleContext(Handle_callContext.class, 0);
        }

        public VarContext var() {
            return getRuleContext(VarContext.class, 0);
        }

        public TerminalNode Read() {
            return getToken(luluParser.Read, 0);
        }

        public TerminalNode Write() {
            return getToken(luluParser.Write, 0);
        }

        public ExprContext expr() {
            return getRuleContext(ExprContext.class, 0);
        }

        public Func_callContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_func_call;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterFunc_call(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitFunc_call(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitFunc_call(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Func_callContext func_call() throws RecognitionException {
        Func_callContext _localctx = new Func_callContext(_ctx, getState());
        enterRule(_localctx, 40, RULE_func_call);
        try {
            setState(362);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
                case This:
                case Super:
                case ID:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(351);
                    _errHandler.sync(this);
                    switch (getInterpreter().adaptivePredict(_input, 34, _ctx)) {
                        case 1: {
                            setState(348);
                            var();
                            setState(349);
                            match(T__10);
                        }
                        break;
                    }
                    setState(353);
                    handle_call();
                }
                break;
                case Read:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(354);
                    match(Read);
                    setState(355);
                    match(T__2);
                    setState(356);
                    match(T__3);
                }
                break;
                case Write:
                    enterOuterAlt(_localctx, 3);
                {
                    setState(357);
                    match(Write);
                    setState(358);
                    match(T__2);
                    setState(359);
                    expr(0);
                    setState(360);
                    match(T__3);
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ListContext extends ParserRuleContext {
        public List<ExprContext> expr() {
            return getRuleContexts(ExprContext.class);
        }

        public ExprContext expr(int i) {
            return getRuleContext(ExprContext.class, i);
        }

        public List<ListContext> list() {
            return getRuleContexts(ListContext.class);
        }

        public ListContext list(int i) {
            return getRuleContext(ListContext.class, i);
        }

        public ListContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_list;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterList(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitList(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitList(this);
            else return visitor.visitChildren(this);
        }
    }

    public final ListContext list() throws RecognitionException {
        ListContext _localctx = new ListContext(_ctx, getState());
        enterRule(_localctx, 42, RULE_list);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(364);
                match(T__6);
                setState(367);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 36, _ctx)) {
                    case 1: {
                        setState(365);
                        expr(0);
                    }
                    break;
                    case 2: {
                        setState(366);
                        list();
                    }
                    break;
                }
                setState(376);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == T__8) {
                    {
                        {
                            setState(369);
                            match(T__8);
                            setState(372);
                            _errHandler.sync(this);
                            switch (getInterpreter().adaptivePredict(_input, 37, _ctx)) {
                                case 1: {
                                    setState(370);
                                    expr(0);
                                }
                                break;
                                case 2: {
                                    setState(371);
                                    list();
                                }
                                break;
                            }
                        }
                    }
                    setState(378);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(379);
                match(T__7);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Handle_callContext extends ParserRuleContext {
        public TerminalNode ID() {
            return getToken(luluParser.ID, 0);
        }

        public ParamsContext params() {
            return getRuleContext(ParamsContext.class, 0);
        }

        public Handle_callContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_handle_call;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterHandle_call(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitHandle_call(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitHandle_call(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Handle_callContext handle_call() throws RecognitionException {
        Handle_callContext _localctx = new Handle_callContext(_ctx, getState());
        enterRule(_localctx, 44, RULE_handle_call);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(381);
                match(ID);
                setState(382);
                match(T__2);
                setState(384);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__6) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << This) | (1L << Super) | (1L << Read) | (1L << Write) | (1L << Nil) | (1L << Allocate) | (1L << Int_val) | (1L << Float_val) | (1L << String_val) | (1L << Bool_val) | (1L << ID))) != 0)) {
                    {
                        setState(383);
                        params();
                    }
                }

                setState(386);
                match(T__3);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ParamsContext extends ParserRuleContext {
        public ExprContext expr() {
            return getRuleContext(ExprContext.class, 0);
        }

        public ParamsContext params() {
            return getRuleContext(ParamsContext.class, 0);
        }

        public ParamsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_params;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterParams(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitParams(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitParams(this);
            else return visitor.visitChildren(this);
        }
    }

    public final ParamsContext params() throws RecognitionException {
        ParamsContext _localctx = new ParamsContext(_ctx, getState());
        enterRule(_localctx, 46, RULE_params);
        try {
            setState(393);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 40, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(388);
                    expr(0);
                }
                break;
                case 2:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(389);
                    expr(0);
                    setState(390);
                    match(T__8);
                    setState(391);
                    params();
                }
                break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Cond_stmtContext extends ParserRuleContext {
        public TerminalNode If() {
            return getToken(luluParser.If, 0);
        }

        public ExprContext expr() {
            return getRuleContext(ExprContext.class, 0);
        }

        public List<BlockContext> block() {
            return getRuleContexts(BlockContext.class);
        }

        public BlockContext block(int i) {
            return getRuleContext(BlockContext.class, i);
        }

        public List<StmtContext> stmt() {
            return getRuleContexts(StmtContext.class);
        }

        public StmtContext stmt(int i) {
            return getRuleContext(StmtContext.class, i);
        }

        public TerminalNode Else() {
            return getToken(luluParser.Else, 0);
        }

        public TerminalNode Switch() {
            return getToken(luluParser.Switch, 0);
        }

        public VarContext var() {
            return getRuleContext(VarContext.class, 0);
        }

        public Switch_bodyContext switch_body() {
            return getRuleContext(Switch_bodyContext.class, 0);
        }

        public Cond_stmtContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_cond_stmt;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterCond_stmt(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitCond_stmt(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitCond_stmt(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Cond_stmtContext cond_stmt() throws RecognitionException {
        Cond_stmtContext _localctx = new Cond_stmtContext(_ctx, getState());
        enterRule(_localctx, 48, RULE_cond_stmt);
        try {
            setState(414);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
                case If:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(395);
                    match(If);
                    setState(396);
                    expr(0);
                    setState(399);
                    _errHandler.sync(this);
                    switch (_input.LA(1)) {
                        case T__0: {
                            setState(397);
                            block();
                        }
                        break;
                        case T__2:
                        case If:
                        case Switch:
                        case Break:
                        case For:
                        case While:
                        case Continue:
                        case This:
                        case Super:
                        case Read:
                        case Write:
                        case Destruct:
                        case ID: {
                            setState(398);
                            stmt();
                        }
                        break;
                        default:
                            throw new NoViableAltException(this);
                    }
                    setState(406);
                    _errHandler.sync(this);
                    switch (getInterpreter().adaptivePredict(_input, 43, _ctx)) {
                        case 1: {
                            setState(401);
                            match(Else);
                            setState(404);
                            _errHandler.sync(this);
                            switch (_input.LA(1)) {
                                case T__0: {
                                    setState(402);
                                    block();
                                }
                                break;
                                case T__2:
                                case If:
                                case Switch:
                                case Break:
                                case For:
                                case While:
                                case Continue:
                                case This:
                                case Super:
                                case Read:
                                case Write:
                                case Destruct:
                                case ID: {
                                    setState(403);
                                    stmt();
                                }
                                break;
                                default:
                                    throw new NoViableAltException(this);
                            }
                        }
                        break;
                    }
                }
                break;
                case Switch:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(408);
                    match(Switch);
                    setState(409);
                    var();
                    setState(410);
                    match(T__0);
                    setState(411);
                    switch_body();
                    setState(412);
                    match(T__1);
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Switch_bodyContext extends ParserRuleContext {
        public List<TerminalNode> Caseof() {
            return getTokens(luluParser.Caseof);
        }

        public TerminalNode Caseof(int i) {
            return getToken(luluParser.Caseof, i);
        }

        public List<TerminalNode> Int_val() {
            return getTokens(luluParser.Int_val);
        }

        public TerminalNode Int_val(int i) {
            return getToken(luluParser.Int_val, i);
        }

        public List<BlockContext> block() {
            return getRuleContexts(BlockContext.class);
        }

        public BlockContext block(int i) {
            return getRuleContext(BlockContext.class, i);
        }

        public TerminalNode Default() {
            return getToken(luluParser.Default, 0);
        }

        public Switch_bodyContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_switch_body;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterSwitch_body(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitSwitch_body(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitSwitch_body(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Switch_bodyContext switch_body() throws RecognitionException {
        Switch_bodyContext _localctx = new Switch_bodyContext(_ctx, getState());
        enterRule(_localctx, 50, RULE_switch_body);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(420);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(416);
                            match(Caseof);
                            setState(417);
                            match(Int_val);
                            setState(418);
                            match(T__9);
                            setState(419);
                            block();
                        }
                    }
                    setState(422);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while (_la == Caseof);
                setState(427);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == Default) {
                    {
                        setState(424);
                        match(Default);
                        setState(425);
                        match(T__9);
                        setState(426);
                        block();
                    }
                }

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Loop_stmtContext extends ParserRuleContext {
        public TerminalNode For() {
            return getToken(luluParser.For, 0);
        }

        public ExprContext expr() {
            return getRuleContext(ExprContext.class, 0);
        }

        public BlockContext block() {
            return getRuleContext(BlockContext.class, 0);
        }

        public List<AssignContext> assign() {
            return getRuleContexts(AssignContext.class);
        }

        public AssignContext assign(int i) {
            return getRuleContext(AssignContext.class, i);
        }

        public TypeContext type() {
            return getRuleContext(TypeContext.class, 0);
        }

        public TerminalNode While() {
            return getToken(luluParser.While, 0);
        }

        public Loop_stmtContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_loop_stmt;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterLoop_stmt(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitLoop_stmt(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitLoop_stmt(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Loop_stmtContext loop_stmt() throws RecognitionException {
        Loop_stmtContext _localctx = new Loop_stmtContext(_ctx, getState());
        enterRule(_localctx, 52, RULE_loop_stmt);
        int _la;
        try {
            setState(448);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
                case For:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(429);
                    match(For);
                    setState(434);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << Int) | (1L << Float) | (1L << Bool) | (1L << String) | (1L << This) | (1L << Super) | (1L << ID))) != 0)) {
                        {
                            setState(431);
                            _errHandler.sync(this);
                            switch (getInterpreter().adaptivePredict(_input, 47, _ctx)) {
                                case 1: {
                                    setState(430);
                                    type();
                                }
                                break;
                            }
                            setState(433);
                            assign();
                        }
                    }

                    setState(436);
                    match(T__5);
                    setState(437);
                    expr(0);
                    setState(438);
                    match(T__5);
                    setState(440);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << This) | (1L << Super) | (1L << ID))) != 0)) {
                        {
                            setState(439);
                            assign();
                        }
                    }

                    setState(442);
                    block();
                }
                break;
                case While:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(444);
                    match(While);
                    setState(445);
                    expr(0);
                    setState(446);
                    block();
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class TypeContext extends ParserRuleContext {
        public TerminalNode Int() {
            return getToken(luluParser.Int, 0);
        }

        public TerminalNode Float() {
            return getToken(luluParser.Float, 0);
        }

        public TerminalNode Bool() {
            return getToken(luluParser.Bool, 0);
        }

        public TerminalNode String() {
            return getToken(luluParser.String, 0);
        }

        public TerminalNode ID() {
            return getToken(luluParser.ID, 0);
        }

        public TypeContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_type;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterType(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitType(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitType(this);
            else return visitor.visitChildren(this);
        }
    }

    public final TypeContext type() throws RecognitionException {
        TypeContext _localctx = new TypeContext(_ctx, getState());
        enterRule(_localctx, 54, RULE_type);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(450);
                _la = _input.LA(1);
                if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Int) | (1L << Float) | (1L << Bool) | (1L << String) | (1L << ID))) != 0))) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Const_valContext extends ParserRuleContext {
        public TerminalNode Int_val() {
            return getToken(luluParser.Int_val, 0);
        }

        public TerminalNode Float_val() {
            return getToken(luluParser.Float_val, 0);
        }

        public TerminalNode String_val() {
            return getToken(luluParser.String_val, 0);
        }

        public TerminalNode Bool_val() {
            return getToken(luluParser.Bool_val, 0);
        }

        public Const_valContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_const_val;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterConst_val(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitConst_val(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitConst_val(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Const_valContext const_val() throws RecognitionException {
        Const_valContext _localctx = new Const_valContext(_ctx, getState());
        enterRule(_localctx, 56, RULE_const_val);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(452);
                _la = _input.LA(1);
                if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Int_val) | (1L << Float_val) | (1L << String_val) | (1L << Bool_val))) != 0))) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Unary_opContext extends ParserRuleContext {
        public Unary_opContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_unary_op;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterUnary_op(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitUnary_op(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitUnary_op(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Unary_opContext unary_op() throws RecognitionException {
        Unary_opContext _localctx = new Unary_opContext(_ctx, getState());
        enterRule(_localctx, 58, RULE_unary_op);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(454);
                _la = _input.LA(1);
                if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__11) | (1L << T__12) | (1L << T__13))) != 0))) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Op1Context extends ParserRuleContext {
        public Op1Context(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_op1;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterOp1(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitOp1(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitOp1(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Op1Context op1() throws RecognitionException {
        Op1Context _localctx = new Op1Context(_ctx, getState());
        enterRule(_localctx, 60, RULE_op1);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(456);
                _la = _input.LA(1);
                if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__15) | (1L << T__16))) != 0))) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Op2Context extends ParserRuleContext {
        public Op2Context(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_op2;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterOp2(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitOp2(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitOp2(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Op2Context op2() throws RecognitionException {
        Op2Context _localctx = new Op2Context(_ctx, getState());
        enterRule(_localctx, 62, RULE_op2);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(458);
                _la = _input.LA(1);
                if (!(_la == T__13 || _la == T__17)) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Op3Context extends ParserRuleContext {
        public Op3Context(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_op3;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterOp3(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitOp3(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitOp3(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Op3Context op3() throws RecognitionException {
        Op3Context _localctx = new Op3Context(_ctx, getState());
        enterRule(_localctx, 64, RULE_op3);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(460);
                _la = _input.LA(1);
                if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21))) != 0))) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Op4Context extends ParserRuleContext {
        public Op4Context(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_op4;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterOp4(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitOp4(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitOp4(this);
            else return visitor.visitChildren(this);
        }
    }

    public final Op4Context op4() throws RecognitionException {
        Op4Context _localctx = new Op4Context(_ctx, getState());
        enterRule(_localctx, 66, RULE_op4);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(462);
                _la = _input.LA(1);
                if (!(_la == T__22 || _la == T__23)) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class BitwiseContext extends ParserRuleContext {
        public BitwiseContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_bitwise;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterBitwise(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitBitwise(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitBitwise(this);
            else return visitor.visitChildren(this);
        }
    }

    public final BitwiseContext bitwise() throws RecognitionException {
        BitwiseContext _localctx = new BitwiseContext(_ctx, getState());
        enterRule(_localctx, 68, RULE_bitwise);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(464);
                _la = _input.LA(1);
                if (!(_la == T__24 || _la == T__25)) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class LogicalContext extends ParserRuleContext {
        public LogicalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_logical;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).enterLogical(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof luluListener) ((luluListener) listener).exitLogical(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof luluVisitor) return ((luluVisitor<? extends T>) visitor).visitLogical(this);
            else return visitor.visitChildren(this);
        }
    }

    public final LogicalContext logical() throws RecognitionException {
        LogicalContext _localctx = new LogicalContext(_ctx, getState());
        enterRule(_localctx, 70, RULE_logical);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(466);
                _la = _input.LA(1);
                if (!(_la == T__26 || _la == T__27)) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
        switch (ruleIndex) {
            case 4:
                return args_sempred((ArgsContext) _localctx, predIndex);
            case 5:
                return args_var_sempred((Args_varContext) _localctx, predIndex);
            case 19:
                return expr_sempred((ExprContext) _localctx, predIndex);
        }
        return true;
    }

    private boolean args_sempred(ArgsContext _localctx, int predIndex) {
        switch (predIndex) {
            case 0:
                return precpred(_ctx, 1);
        }
        return true;
    }

    private boolean args_var_sempred(Args_varContext _localctx, int predIndex) {
        switch (predIndex) {
            case 1:
                return precpred(_ctx, 1);
        }
        return true;
    }

    private boolean expr_sempred(ExprContext _localctx, int predIndex) {
        switch (predIndex) {
            case 2:
                return precpred(_ctx, 13);
            case 3:
                return precpred(_ctx, 12);
            case 4:
                return precpred(_ctx, 11);
            case 5:
                return precpred(_ctx, 10);
            case 6:
                return precpred(_ctx, 9);
            case 7:
                return precpred(_ctx, 8);
        }
        return true;
    }

    public static final String _serializedATN =
            "\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3@\u01d7\4\2\t\2\4" +
                    "\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t" +
                    "\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22" +
                    "\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31" +
                    "\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!" +
                    "\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\3\2\5\2L\n\2\3\2\6\2O\n\2\r\2\16\2P\3\2" +
                    "\3\2\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\6\4]\n\4\r\4\16\4^\3\4\3\4\3\5\3" +
                    "\5\3\5\3\5\3\5\5\5h\n\5\3\5\3\5\3\5\3\5\5\5n\n\5\3\5\3\5\3\5\3\6\3\6\3" +
                    "\6\3\6\7\6w\n\6\f\6\16\6z\13\6\3\6\3\6\3\6\3\6\3\6\7\6\u0081\n\6\f\6\16" +
                    "\6\u0084\13\6\7\6\u0086\n\6\f\6\16\6\u0089\13\6\3\7\3\7\3\7\3\7\7\7\u008f" +
                    "\n\7\f\7\16\7\u0092\13\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7\u009b\n\7\f\7" +
                    "\16\7\u009e\13\7\3\7\3\7\7\7\u00a2\n\7\f\7\16\7\u00a5\13\7\3\b\3\b\3\b" +
                    "\3\t\5\t\u00ab\n\t\3\t\3\t\3\t\3\t\7\t\u00b1\n\t\f\t\16\t\u00b4\13\t\3" +
                    "\t\3\t\3\n\3\n\3\n\5\n\u00bb\n\n\3\13\3\13\5\13\u00bf\n\13\3\f\3\f\3\f" +
                    "\3\f\5\f\u00c5\n\f\3\f\3\f\6\f\u00c9\n\f\r\f\16\f\u00ca\3\f\3\f\3\r\5" +
                    "\r\u00d0\n\r\3\r\3\r\5\r\u00d4\n\r\3\16\3\16\3\17\3\17\3\17\3\17\3\17" +
                    "\5\17\u00dd\n\17\3\17\3\17\3\17\3\17\5\17\u00e3\n\17\3\17\3\17\3\17\3" +
                    "\20\3\20\3\20\7\20\u00eb\n\20\f\20\16\20\u00ee\13\20\3\20\3\20\3\21\3" +
                    "\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\7" +
                    "\21\u0101\n\21\f\21\16\21\u0104\13\21\3\21\3\21\5\21\u0108\n\21\3\22\3" +
                    "\22\3\22\3\22\3\22\7\22\u010f\n\22\f\22\16\22\u0112\13\22\3\22\3\22\5" +
                    "\22\u0116\n\22\3\22\3\22\3\22\3\23\3\23\5\23\u011d\n\23\3\23\3\23\3\23" +
                    "\7\23\u0122\n\23\f\23\16\23\u0125\13\23\3\24\3\24\3\24\3\24\3\24\7\24" +
                    "\u012c\n\24\f\24\16\24\u012f\13\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25" +
                    "\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u0140\n\25\3\25\3\25\3\25" +
                    "\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25" +
                    "\3\25\3\25\3\25\3\25\3\25\3\25\3\25\7\25\u015a\n\25\f\25\16\25\u015d\13" +
                    "\25\3\26\3\26\3\26\5\26\u0162\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26" +
                    "\3\26\3\26\5\26\u016d\n\26\3\27\3\27\3\27\5\27\u0172\n\27\3\27\3\27\3" +
                    "\27\5\27\u0177\n\27\7\27\u0179\n\27\f\27\16\27\u017c\13\27\3\27\3\27\3" +
                    "\30\3\30\3\30\5\30\u0183\n\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\5\31" +
                    "\u018c\n\31\3\32\3\32\3\32\3\32\5\32\u0192\n\32\3\32\3\32\3\32\5\32\u0197" +
                    "\n\32\5\32\u0199\n\32\3\32\3\32\3\32\3\32\3\32\3\32\5\32\u01a1\n\32\3" +
                    "\33\3\33\3\33\3\33\6\33\u01a7\n\33\r\33\16\33\u01a8\3\33\3\33\3\33\5\33" +
                    "\u01ae\n\33\3\34\3\34\5\34\u01b2\n\34\3\34\5\34\u01b5\n\34\3\34\3\34\3" +
                    "\34\3\34\5\34\u01bb\n\34\3\34\3\34\3\34\3\34\3\34\3\34\5\34\u01c3\n\34" +
                    "\3\35\3\35\3\36\3\36\3\37\3\37\3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3" +
                    "%\2\5\n\f(&\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\66" +
                    "8:<>@BDFH\2\r\3\29;\3\2\60\61\4\2!$@@\3\2<?\3\2\16\20\3\2\21\23\4\2\20" +
                    "\20\24\24\3\2\25\30\3\2\31\32\3\2\33\34\3\2\35\36\2\u01f7\2K\3\2\2\2\4" +
                    "T\3\2\2\2\6W\3\2\2\2\bg\3\2\2\2\nr\3\2\2\2\f\u008a\3\2\2\2\16\u00a6\3" +
                    "\2\2\2\20\u00aa\3\2\2\2\22\u00b7\3\2\2\2\24\u00be\3\2\2\2\26\u00c0\3\2" +
                    "\2\2\30\u00cf\3\2\2\2\32\u00d5\3\2\2\2\34\u00dc\3\2\2\2\36\u00e7\3\2\2" +
                    "\2 \u0107\3\2\2\2\"\u0115\3\2\2\2$\u011c\3\2\2\2&\u0126\3\2\2\2(\u013f" +
                    "\3\2\2\2*\u016c\3\2\2\2,\u016e\3\2\2\2.\u017f\3\2\2\2\60\u018b\3\2\2\2" +
                    "\62\u01a0\3\2\2\2\64\u01a6\3\2\2\2\66\u01c2\3\2\2\28\u01c4\3\2\2\2:\u01c6" +
                    "\3\2\2\2<\u01c8\3\2\2\2>\u01ca\3\2\2\2@\u01cc\3\2\2\2B\u01ce\3\2\2\2D" +
                    "\u01d0\3\2\2\2F\u01d2\3\2\2\2H\u01d4\3\2\2\2JL\5\6\4\2KJ\3\2\2\2KL\3\2" +
                    "\2\2LN\3\2\2\2MO\5\24\13\2NM\3\2\2\2OP\3\2\2\2PN\3\2\2\2PQ\3\2\2\2QR\3" +
                    "\2\2\2RS\7\2\2\3S\3\3\2\2\2TU\5(\25\2UV\7\2\2\3V\5\3\2\2\2WX\7\63\2\2" +
                    "X\\\7\3\2\2Y]\5\b\5\2Z]\5\16\b\2[]\5\20\t\2\\Y\3\2\2\2\\Z\3\2\2\2\\[\3" +
                    "\2\2\2]^\3\2\2\2^\\\3\2\2\2^_\3\2\2\2_`\3\2\2\2`a\7\4\2\2a\7\3\2\2\2b" +
                    "c\7\5\2\2cd\5\n\6\2de\7\6\2\2ef\7\7\2\2fh\3\2\2\2gb\3\2\2\2gh\3\2\2\2" +
                    "hi\3\2\2\2ij\7@\2\2jm\7\5\2\2kn\5\n\6\2ln\5\f\7\2mk\3\2\2\2ml\3\2\2\2" +
                    "mn\3\2\2\2no\3\2\2\2op\7\6\2\2pq\7\b\2\2q\t\3\2\2\2rs\b\6\1\2sx\58\35" +
                    "\2tu\7\t\2\2uw\7\n\2\2vt\3\2\2\2wz\3\2\2\2xv\3\2\2\2xy\3\2\2\2y\u0087" +
                    "\3\2\2\2zx\3\2\2\2{|\f\3\2\2|}\7\13\2\2}\u0082\58\35\2~\177\7\t\2\2\177" +
                    "\u0081\7\n\2\2\u0080~\3\2\2\2\u0081\u0084\3\2\2\2\u0082\u0080\3\2\2\2" +
                    "\u0082\u0083\3\2\2\2\u0083\u0086\3\2\2\2\u0084\u0082\3\2\2\2\u0085{\3" +
                    "\2\2\2\u0086\u0089\3\2\2\2\u0087\u0085\3\2\2\2\u0087\u0088\3\2\2\2\u0088" +
                    "\13\3\2\2\2\u0089\u0087\3\2\2\2\u008a\u008b\b\7\1\2\u008b\u0090\58\35" +
                    "\2\u008c\u008d\7\t\2\2\u008d\u008f\7\n\2\2\u008e\u008c\3\2\2\2\u008f\u0092" +
                    "\3\2\2\2\u0090\u008e\3\2\2\2\u0090\u0091\3\2\2\2\u0091\u0093\3\2\2\2\u0092" +
                    "\u0090\3\2\2\2\u0093\u0094\7@\2\2\u0094\u00a3\3\2\2\2\u0095\u0096\f\3" +
                    "\2\2\u0096\u0097\7\13\2\2\u0097\u009c\58\35\2\u0098\u0099\7\t\2\2\u0099" +
                    "\u009b\7\n\2\2\u009a\u0098\3\2\2\2\u009b\u009e\3\2\2\2\u009c\u009a\3\2" +
                    "\2\2\u009c\u009d\3\2\2\2\u009d\u009f\3\2\2\2\u009e\u009c\3\2\2\2\u009f" +
                    "\u00a0\7@\2\2\u00a0\u00a2\3\2\2\2\u00a1\u0095\3\2\2\2\u00a2\u00a5\3\2" +
                    "\2\2\u00a3\u00a1\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\r\3\2\2\2\u00a5\u00a3" +
                    "\3\2\2\2\u00a6\u00a7\7@\2\2\u00a7\u00a8\7\b\2\2\u00a8\17\3\2\2\2\u00a9" +
                    "\u00ab\7.\2\2\u00aa\u00a9\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00ac\3\2" +
                    "\2\2\u00ac\u00ad\58\35\2\u00ad\u00b2\5\22\n\2\u00ae\u00af\7\13\2\2\u00af" +
                    "\u00b1\5\22\n\2\u00b0\u00ae\3\2\2\2\u00b1\u00b4\3\2\2\2\u00b2\u00b0\3" +
                    "\2\2\2\u00b2\u00b3\3\2\2\2\u00b3\u00b5\3\2\2\2\u00b4\u00b2\3\2\2\2\u00b5" +
                    "\u00b6\7\b\2\2\u00b6\21\3\2\2\2\u00b7\u00ba\5&\24\2\u00b8\u00b9\7\7\2" +
                    "\2\u00b9\u00bb\5(\25\2\u00ba\u00b8\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb\23" +
                    "\3\2\2\2\u00bc\u00bf\5\26\f\2\u00bd\u00bf\5\34\17\2\u00be\u00bc\3\2\2" +
                    "\2\u00be\u00bd\3\2\2\2\u00bf\25\3\2\2\2\u00c0\u00c1\7\62\2\2\u00c1\u00c4" +
                    "\7@\2\2\u00c2\u00c3\7\f\2\2\u00c3\u00c5\7@\2\2\u00c4\u00c2\3\2\2\2\u00c4" +
                    "\u00c5\3\2\2\2\u00c5\u00c6\3\2\2\2\u00c6\u00c8\7\3\2\2\u00c7\u00c9\5\30" +
                    "\r\2\u00c8\u00c7\3\2\2\2\u00c9\u00ca\3\2\2\2\u00ca\u00c8\3\2\2\2\u00ca" +
                    "\u00cb\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\u00cd\7\4\2\2\u00cd\27\3\2\2" +
                    "\2\u00ce\u00d0\5\32\16\2\u00cf\u00ce\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0" +
                    "\u00d3\3\2\2\2\u00d1\u00d4\5\20\t\2\u00d2\u00d4\5\34\17\2\u00d3\u00d1" +
                    "\3\2\2\2\u00d3\u00d2\3\2\2\2\u00d4\31\3\2\2\2\u00d5\u00d6\t\2\2\2\u00d6" +
                    "\33\3\2\2\2\u00d7\u00d8\7\5\2\2\u00d8\u00d9\5\f\7\2\u00d9\u00da\7\6\2" +
                    "\2\u00da\u00db\7\7\2\2\u00db\u00dd\3\2\2\2\u00dc\u00d7\3\2\2\2\u00dc\u00dd" +
                    "\3\2\2\2\u00dd\u00de\3\2\2\2\u00de\u00df\7%\2\2\u00df\u00e0\7@\2\2\u00e0" +
                    "\u00e2\7\5\2\2\u00e1\u00e3\5\f\7\2\u00e2\u00e1\3\2\2\2\u00e2\u00e3\3\2" +
                    "\2\2\u00e3\u00e4\3\2\2\2\u00e4\u00e5\7\6\2\2\u00e5\u00e6\5\36\20\2\u00e6" +
                    "\35\3\2\2\2\u00e7\u00ec\7\3\2\2\u00e8\u00eb\5\20\t\2\u00e9\u00eb\5 \21" +
                    "\2\u00ea\u00e8\3\2\2\2\u00ea\u00e9\3\2\2\2\u00eb\u00ee\3\2\2\2\u00ec\u00ea" +
                    "\3\2\2\2\u00ec\u00ed\3\2\2\2\u00ed\u00ef\3\2\2\2\u00ee\u00ec\3\2\2\2\u00ef" +
                    "\u00f0\7\4\2\2\u00f0\37\3\2\2\2\u00f1\u00f2\5\"\22\2\u00f2\u00f3\7\b\2" +
                    "\2\u00f3\u0108\3\2\2\2\u00f4\u00f5\5*\26\2\u00f5\u00f6\7\b\2\2\u00f6\u0108" +
                    "\3\2\2\2\u00f7\u0108\5\62\32\2\u00f8\u0108\5\66\34\2\u00f9\u00fa\7*\2" +
                    "\2\u00fa\u0108\7\b\2\2\u00fb\u00fc\7-\2\2\u00fc\u0108\7\b\2\2\u00fd\u0102" +
                    "\7\67\2\2\u00fe\u00ff\7\t\2\2\u00ff\u0101\7\n\2\2\u0100\u00fe\3\2\2\2" +
                    "\u0101\u0104\3\2\2\2\u0102\u0100\3\2\2\2\u0102\u0103\3\2\2\2\u0103\u0105" +
                    "\3\2\2\2\u0104\u0102\3\2\2\2\u0105\u0106\7@\2\2\u0106\u0108\7\b\2\2\u0107" +
                    "\u00f1\3\2\2\2\u0107\u00f4\3\2\2\2\u0107\u00f7\3\2\2\2\u0107\u00f8\3\2" +
                    "\2\2\u0107\u00f9\3\2\2\2\u0107\u00fb\3\2\2\2\u0107\u00fd\3\2\2\2\u0108" +
                    "!\3\2\2\2\u0109\u0116\5$\23\2\u010a\u010b\7\5\2\2\u010b\u0110\5$\23\2" +
                    "\u010c\u010d\7\13\2\2\u010d\u010f\5$\23\2\u010e\u010c\3\2\2\2\u010f\u0112" +
                    "\3\2\2\2\u0110\u010e\3\2\2\2\u0110\u0111\3\2\2\2\u0111\u0113\3\2\2\2\u0112" +
                    "\u0110\3\2\2\2\u0113\u0114\7\6\2\2\u0114\u0116\3\2\2\2\u0115\u0109\3\2" +
                    "\2\2\u0115\u010a\3\2\2\2\u0116\u0117\3\2\2\2\u0117\u0118\7\7\2\2\u0118" +
                    "\u0119\5(\25\2\u0119#\3\2\2\2\u011a\u011b\t\3\2\2\u011b\u011d\7\r\2\2" +
                    "\u011c\u011a\3\2\2\2\u011c\u011d\3\2\2\2\u011d\u011e\3\2\2\2\u011e\u0123" +
                    "\5&\24\2\u011f\u0120\7\r\2\2\u0120\u0122\5&\24\2\u0121\u011f\3\2\2\2\u0122" +
                    "\u0125\3\2\2\2\u0123\u0121\3\2\2\2\u0123\u0124\3\2\2\2\u0124%\3\2\2\2" +
                    "\u0125\u0123\3\2\2\2\u0126\u012d\7@\2\2\u0127\u0128\7\t\2\2\u0128\u0129" +
                    "\5(\25\2\u0129\u012a\7\n\2\2\u012a\u012c\3\2\2\2\u012b\u0127\3\2\2\2\u012c" +
                    "\u012f\3\2\2\2\u012d\u012b\3\2\2\2\u012d\u012e\3\2\2\2\u012e\'\3\2\2\2" +
                    "\u012f\u012d\3\2\2\2\u0130\u0131\b\25\1\2\u0131\u0132\5<\37\2\u0132\u0133" +
                    "\5(\25\20\u0133\u0140\3\2\2\2\u0134\u0135\7\5\2\2\u0135\u0136\5(\25\2" +
                    "\u0136\u0137\7\6\2\2\u0137\u0140\3\2\2\2\u0138\u0140\5:\36\2\u0139\u013a" +
                    "\78\2\2\u013a\u0140\5.\30\2\u013b\u0140\5*\26\2\u013c\u0140\5$\23\2\u013d" +
                    "\u0140\5,\27\2\u013e\u0140\7\66\2\2\u013f\u0130\3\2\2\2\u013f\u0134\3" +
                    "\2\2\2\u013f\u0138\3\2\2\2\u013f\u0139\3\2\2\2\u013f\u013b\3\2\2\2\u013f" +
                    "\u013c\3\2\2\2\u013f\u013d\3\2\2\2\u013f\u013e\3\2\2\2\u0140\u015b\3\2" +
                    "\2\2\u0141\u0142\f\17\2\2\u0142\u0143\5> \2\u0143\u0144\5(\25\20\u0144" +
                    "\u015a\3\2\2\2\u0145\u0146\f\16\2\2\u0146\u0147\5@!\2\u0147\u0148\5(\25" +
                    "\17\u0148\u015a\3\2\2\2\u0149\u014a\f\r\2\2\u014a\u014b\5B\"\2\u014b\u014c" +
                    "\5(\25\16\u014c\u015a\3\2\2\2\u014d\u014e\f\f\2\2\u014e\u014f\5D#\2\u014f" +
                    "\u0150\5(\25\r\u0150\u015a\3\2\2\2\u0151\u0152\f\13\2\2\u0152\u0153\5" +
                    "F$\2\u0153\u0154\5(\25\f\u0154\u015a\3\2\2\2\u0155\u0156\f\n\2\2\u0156" +
                    "\u0157\5H%\2\u0157\u0158\5(\25\13\u0158\u015a\3\2\2\2\u0159\u0141\3\2" +
                    "\2\2\u0159\u0145\3\2\2\2\u0159\u0149\3\2\2\2\u0159\u014d\3\2\2\2\u0159" +
                    "\u0151\3\2\2\2\u0159\u0155\3\2\2\2\u015a\u015d\3\2\2\2\u015b\u0159\3\2" +
                    "\2\2\u015b\u015c\3\2\2\2\u015c)\3\2\2\2\u015d\u015b\3\2\2\2\u015e\u015f" +
                    "\5$\23\2\u015f\u0160\7\r\2\2\u0160\u0162\3\2\2\2\u0161\u015e\3\2\2\2\u0161" +
                    "\u0162\3\2\2\2\u0162\u0163\3\2\2\2\u0163\u016d\5.\30\2\u0164\u0165\7\64" +
                    "\2\2\u0165\u0166\7\5\2\2\u0166\u016d\7\6\2\2\u0167\u0168\7\65\2\2\u0168" +
                    "\u0169\7\5\2\2\u0169\u016a\5(\25\2\u016a\u016b\7\6\2\2\u016b\u016d\3\2" +
                    "\2\2\u016c\u0161\3\2\2\2\u016c\u0164\3\2\2\2\u016c\u0167\3\2\2\2\u016d" +
                    "+\3\2\2\2\u016e\u0171\7\t\2\2\u016f\u0172\5(\25\2\u0170\u0172\5,\27\2" +
                    "\u0171\u016f\3\2\2\2\u0171\u0170\3\2\2\2\u0172\u017a\3\2\2\2\u0173\u0176" +
                    "\7\13\2\2\u0174\u0177\5(\25\2\u0175\u0177\5,\27\2\u0176\u0174\3\2\2\2" +
                    "\u0176\u0175\3\2\2\2\u0177\u0179\3\2\2\2\u0178\u0173\3\2\2\2\u0179\u017c" +
                    "\3\2\2\2\u017a\u0178\3\2\2\2\u017a\u017b\3\2\2\2\u017b\u017d\3\2\2\2\u017c" +
                    "\u017a\3\2\2\2\u017d\u017e\7\n\2\2\u017e-\3\2\2\2\u017f\u0180\7@\2\2\u0180" +
                    "\u0182\7\5\2\2\u0181\u0183\5\60\31\2\u0182\u0181\3\2\2\2\u0182\u0183\3" +
                    "\2\2\2\u0183\u0184\3\2\2\2\u0184\u0185\7\6\2\2\u0185/\3\2\2\2\u0186\u018c" +
                    "\5(\25\2\u0187\u0188\5(\25\2\u0188\u0189\7\13\2\2\u0189\u018a\5\60\31" +
                    "\2\u018a\u018c\3\2\2\2\u018b\u0186\3\2\2\2\u018b\u0187\3\2\2\2\u018c\61" +
                    "\3\2\2\2\u018d\u018e\7&\2\2\u018e\u0191\5(\25\2\u018f\u0192\5\36\20\2" +
                    "\u0190\u0192\5 \21\2\u0191\u018f\3\2\2\2\u0191\u0190\3\2\2\2\u0192\u0198" +
                    "\3\2\2\2\u0193\u0196\7\'\2\2\u0194\u0197\5\36\20\2\u0195\u0197\5 \21\2" +
                    "\u0196\u0194\3\2\2\2\u0196\u0195\3\2\2\2\u0197\u0199\3\2\2\2\u0198\u0193" +
                    "\3\2\2\2\u0198\u0199\3\2\2\2\u0199\u01a1\3\2\2\2\u019a\u019b\7(\2\2\u019b" +
                    "\u019c\5$\23\2\u019c\u019d\7\3\2\2\u019d\u019e\5\64\33\2\u019e\u019f\7" +
                    "\4\2\2\u019f\u01a1\3\2\2\2\u01a0\u018d\3\2\2\2\u01a0\u019a\3\2\2\2\u01a1" +
                    "\63\3\2\2\2\u01a2\u01a3\7/\2\2\u01a3\u01a4\7<\2\2\u01a4\u01a5\7\f\2\2" +
                    "\u01a5\u01a7\5\36\20\2\u01a6\u01a2\3\2\2\2\u01a7\u01a8\3\2\2\2\u01a8\u01a6" +
                    "\3\2\2\2\u01a8\u01a9\3\2\2\2\u01a9\u01ad\3\2\2\2\u01aa\u01ab\7)\2\2\u01ab" +
                    "\u01ac\7\f\2\2\u01ac\u01ae\5\36\20\2\u01ad\u01aa\3\2\2\2\u01ad\u01ae\3" +
                    "\2\2\2\u01ae\65\3\2\2\2\u01af\u01b4\7+\2\2\u01b0\u01b2\58\35\2\u01b1\u01b0" +
                    "\3\2\2\2\u01b1\u01b2\3\2\2\2\u01b2\u01b3\3\2\2\2\u01b3\u01b5\5\"\22\2" +
                    "\u01b4\u01b1\3\2\2\2\u01b4\u01b5\3\2\2\2\u01b5\u01b6\3\2\2\2\u01b6\u01b7" +
                    "\7\b\2\2\u01b7\u01b8\5(\25\2\u01b8\u01ba\7\b\2\2\u01b9\u01bb\5\"\22\2" +
                    "\u01ba\u01b9\3\2\2\2\u01ba\u01bb\3\2\2\2\u01bb\u01bc\3\2\2\2\u01bc\u01bd" +
                    "\5\36\20\2\u01bd\u01c3\3\2\2\2\u01be\u01bf\7,\2\2\u01bf\u01c0\5(\25\2" +
                    "\u01c0\u01c1\5\36\20\2\u01c1\u01c3\3\2\2\2\u01c2\u01af\3\2\2\2\u01c2\u01be" +
                    "\3\2\2\2\u01c3\67\3\2\2\2\u01c4\u01c5\t\4\2\2\u01c59\3\2\2\2\u01c6\u01c7" +
                    "\t\5\2\2\u01c7;\3\2\2\2\u01c8\u01c9\t\6\2\2\u01c9=\3\2\2\2\u01ca\u01cb" +
                    "\t\7\2\2\u01cb?\3\2\2\2\u01cc\u01cd\t\b\2\2\u01cdA\3\2\2\2\u01ce\u01cf" +
                    "\t\t\2\2\u01cfC\3\2\2\2\u01d0\u01d1\t\n\2\2\u01d1E\3\2\2\2\u01d2\u01d3" +
                    "\t\13\2\2\u01d3G\3\2\2\2\u01d4\u01d5\t\f\2\2\u01d5I\3\2\2\2\65KP\\^gm" +
                    "x\u0082\u0087\u0090\u009c\u00a3\u00aa\u00b2\u00ba\u00be\u00c4\u00ca\u00cf" +
                    "\u00d3\u00dc\u00e2\u00ea\u00ec\u0102\u0107\u0110\u0115\u011c\u0123\u012d" +
                    "\u013f\u0159\u015b\u0161\u016c\u0171\u0176\u017a\u0182\u018b\u0191\u0196" +
                    "\u0198\u01a0\u01a8\u01ad\u01b1\u01b4\u01ba\u01c2";
    public static final ATN _ATN =
            new ATNDeserializer().deserialize(_serializedATN.toCharArray());

    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
        for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
            _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
        }
    }
}