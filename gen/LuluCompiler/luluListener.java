package LuluCompiler;// Generated from /home/amk/IdeaProjects/LULU/src/lulu.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link luluParser}.
 */
public interface luluListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link luluParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(luluParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(luluParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#test}.
	 * @param ctx the parse tree
	 */
	void enterTest(luluParser.TestContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#test}.
	 * @param ctx the parse tree
	 */
	void exitTest(luluParser.TestContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#ft_dcl}.
	 * @param ctx the parse tree
	 */
	void enterFt_dcl(luluParser.Ft_dclContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#ft_dcl}.
	 * @param ctx the parse tree
	 */
	void exitFt_dcl(luluParser.Ft_dclContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#func_dcl}.
	 * @param ctx the parse tree
	 */
	void enterFunc_dcl(luluParser.Func_dclContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#func_dcl}.
	 * @param ctx the parse tree
	 */
	void exitFunc_dcl(luluParser.Func_dclContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#args}.
	 * @param ctx the parse tree
	 */
	void enterArgs(luluParser.ArgsContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#args}.
	 * @param ctx the parse tree
	 */
	void exitArgs(luluParser.ArgsContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#args_var}.
	 * @param ctx the parse tree
	 */
	void enterArgs_var(luluParser.Args_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#args_var}.
	 * @param ctx the parse tree
	 */
	void exitArgs_var(luluParser.Args_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#type_dcl}.
	 * @param ctx the parse tree
	 */
	void enterType_dcl(luluParser.Type_dclContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#type_dcl}.
	 * @param ctx the parse tree
	 */
	void exitType_dcl(luluParser.Type_dclContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#var_def}.
	 * @param ctx the parse tree
	 */
	void enterVar_def(luluParser.Var_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#var_def}.
	 * @param ctx the parse tree
	 */
	void exitVar_def(luluParser.Var_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#var_val}.
	 * @param ctx the parse tree
	 */
	void enterVar_val(luluParser.Var_valContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#var_val}.
	 * @param ctx the parse tree
	 */
	void exitVar_val(luluParser.Var_valContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#ft_def}.
	 * @param ctx the parse tree
	 */
	void enterFt_def(luluParser.Ft_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#ft_def}.
	 * @param ctx the parse tree
	 */
	void exitFt_def(luluParser.Ft_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#type_def}.
	 * @param ctx the parse tree
	 */
	void enterType_def(luluParser.Type_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#type_def}.
	 * @param ctx the parse tree
	 */
	void exitType_def(luluParser.Type_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#component}.
	 * @param ctx the parse tree
	 */
	void enterComponent(luluParser.ComponentContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#component}.
	 * @param ctx the parse tree
	 */
	void exitComponent(luluParser.ComponentContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#access_modifier}.
	 * @param ctx the parse tree
	 */
	void enterAccess_modifier(luluParser.Access_modifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#access_modifier}.
	 * @param ctx the parse tree
	 */
	void exitAccess_modifier(luluParser.Access_modifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#fun_def}.
	 * @param ctx the parse tree
	 */
	void enterFun_def(luluParser.Fun_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#fun_def}.
	 * @param ctx the parse tree
	 */
	void exitFun_def(luluParser.Fun_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(luluParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(luluParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt(luluParser.StmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt(luluParser.StmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#assign}.
	 * @param ctx the parse tree
	 */
	void enterAssign(luluParser.AssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#assign}.
	 * @param ctx the parse tree
	 */
	void exitAssign(luluParser.AssignContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#var}.
	 * @param ctx the parse tree
	 */
	void enterVar(luluParser.VarContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#var}.
	 * @param ctx the parse tree
	 */
	void exitVar(luluParser.VarContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#ref}.
	 * @param ctx the parse tree
	 */
	void enterRef(luluParser.RefContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#ref}.
	 * @param ctx the parse tree
	 */
	void exitRef(luluParser.RefContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(luluParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(luluParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#func_call}.
	 * @param ctx the parse tree
	 */
	void enterFunc_call(luluParser.Func_callContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#func_call}.
	 * @param ctx the parse tree
	 */
	void exitFunc_call(luluParser.Func_callContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#list}.
	 * @param ctx the parse tree
	 */
	void enterList(luluParser.ListContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#list}.
	 * @param ctx the parse tree
	 */
	void exitList(luluParser.ListContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#handle_call}.
	 * @param ctx the parse tree
	 */
	void enterHandle_call(luluParser.Handle_callContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#handle_call}.
	 * @param ctx the parse tree
	 */
	void exitHandle_call(luluParser.Handle_callContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#params}.
	 * @param ctx the parse tree
	 */
	void enterParams(luluParser.ParamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#params}.
	 * @param ctx the parse tree
	 */
	void exitParams(luluParser.ParamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#cond_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCond_stmt(luluParser.Cond_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#cond_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCond_stmt(luluParser.Cond_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#switch_body}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_body(luluParser.Switch_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#switch_body}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_body(luluParser.Switch_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#loop_stmt}.
	 * @param ctx the parse tree
	 */
	void enterLoop_stmt(luluParser.Loop_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#loop_stmt}.
	 * @param ctx the parse tree
	 */
	void exitLoop_stmt(luluParser.Loop_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(luluParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(luluParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#const_val}.
	 * @param ctx the parse tree
	 */
	void enterConst_val(luluParser.Const_valContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#const_val}.
	 * @param ctx the parse tree
	 */
	void exitConst_val(luluParser.Const_valContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#unary_op}.
	 * @param ctx the parse tree
	 */
	void enterUnary_op(luluParser.Unary_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#unary_op}.
	 * @param ctx the parse tree
	 */
	void exitUnary_op(luluParser.Unary_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#op1}.
	 * @param ctx the parse tree
	 */
	void enterOp1(luluParser.Op1Context ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#op1}.
	 * @param ctx the parse tree
	 */
	void exitOp1(luluParser.Op1Context ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#op2}.
	 * @param ctx the parse tree
	 */
	void enterOp2(luluParser.Op2Context ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#op2}.
	 * @param ctx the parse tree
	 */
	void exitOp2(luluParser.Op2Context ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#op3}.
	 * @param ctx the parse tree
	 */
	void enterOp3(luluParser.Op3Context ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#op3}.
	 * @param ctx the parse tree
	 */
	void exitOp3(luluParser.Op3Context ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#op4}.
	 * @param ctx the parse tree
	 */
	void enterOp4(luluParser.Op4Context ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#op4}.
	 * @param ctx the parse tree
	 */
	void exitOp4(luluParser.Op4Context ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#bitwise}.
	 * @param ctx the parse tree
	 */
	void enterBitwise(luluParser.BitwiseContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#bitwise}.
	 * @param ctx the parse tree
	 */
	void exitBitwise(luluParser.BitwiseContext ctx);
	/**
	 * Enter a parse tree produced by {@link luluParser#logical}.
	 * @param ctx the parse tree
	 */
	void enterLogical(luluParser.LogicalContext ctx);
	/**
	 * Exit a parse tree produced by {@link luluParser#logical}.
	 * @param ctx the parse tree
	 */
	void exitLogical(luluParser.LogicalContext ctx);
}