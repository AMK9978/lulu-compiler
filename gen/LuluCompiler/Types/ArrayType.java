package LuluCompiler.Types;

import LuluCompiler.Scope;

import java.util.HashMap;
import java.util.Map;

public class ArrayType extends BaseType {
    private BaseType elementType;
    public static final String NAME = "arr";
    private int dimensions;
    private Map<Integer, Integer> dimension_size = new HashMap<>();

    public ArrayType(BaseType elementType, int dimensions) {
        super(NAME, elementType.getSize());
        this.dimensions = dimensions;
        this.elementType = elementType;
    }

    public BaseType getElementType() {
        return elementType;
    }

    public void setElementType(BaseType elementType) {
        this.elementType = elementType;
    }

    public static String getNAME() {
        return NAME;
    }

    public int getDimensions() {
        return dimensions;
    }

    public void setDimensions(int dimensions) {
        this.dimensions = dimensions;
    }

    @Override
    public boolean isConvertible(BaseType baseType) {
        return false;
    }

    @Override
    public String getScopeName() {
        return null;
    }

    @Override
    public Scope getEnclosingScope() {
        return null;
    }

    @Override
    public Object getDefaultValue() {
        if (elementType instanceof LuluObject)
            return null;
        else {
            //TODO: It should be fixed:
            return elementType.getDefaultValue();
        }
    }
}
