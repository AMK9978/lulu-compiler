package LuluCompiler.Types.PrimitiveTypes;

import LuluCompiler.Scope;
import LuluCompiler.Types.BaseType;

public class STRING extends BaseType {
    public static final String NAME = "string";
    private static final int SIZE = 2;
    private static BaseType instance;

    public static BaseType getInstance() {
        if (instance == null){
            instance = new STRING();
        }
        return instance;
    }

    private STRING() {
        super(NAME, SIZE);
    }

    @Override
    public boolean isConvertible(BaseType baseType) {
        if (baseType.getName().equals(INT.NAME) || baseType.getName().equals(NAME))
            return true;
        return false;
    }

    @Override
    public String getScopeName() {
        return null;
    }

    @Override
    public Scope getEnclosingScope() {
        return null;
    }

    @Override
    public Object getDefaultValue() {
        return "";
    }
}
