package LuluCompiler.Types.PrimitiveTypes;

import LuluCompiler.Scope;
import LuluCompiler.Types.BaseType;

public class INT extends BaseType {
    public static final String NAME = "int";
    private static final int SIZE = 4;
    private static BaseType instance;

    private INT() {
        super(NAME, SIZE);
    }

    @Override
    public boolean isConvertible(BaseType baseType) {
        return false;
    }

    @Override
    public String getScopeName() {
        return null;
    }

    @Override
    public Scope getEnclosingScope() {
        return null;
    }

    @Override
    public Object getDefaultValue() {
        return 0;
    }

    public static BaseType getInstance() {
        if (instance == null){
            instance = new INT();
        }
        return instance;
    }
}
