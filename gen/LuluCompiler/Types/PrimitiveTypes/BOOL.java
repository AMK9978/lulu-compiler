package LuluCompiler.Types.PrimitiveTypes;

import LuluCompiler.Scope;
import LuluCompiler.Types.BaseType;

public class BOOL extends BaseType {
    public static final String NAME = "bool";
    private static final int SIZE = 1;
    private static BaseType instance;

    private BOOL() {
        super(NAME, SIZE);
    }

    @Override
    public boolean isConvertible(BaseType baseType) {
        return false;
    }

    @Override
    public String getScopeName() {
        return null;
    }

    @Override
    public Scope getEnclosingScope() {
        return null;
    }

    @Override
    public Object getDefaultValue() {
        return false;
    }

    public static BaseType getInstance() {
        if (instance == null) {
            instance = new BOOL();
        }
        return instance;
    }
}
