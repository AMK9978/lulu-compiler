package LuluCompiler.Types.PrimitiveTypes;

import LuluCompiler.Scope;
import LuluCompiler.Types.BaseType;

public class FLOAT extends BaseType {
    public static final String NAME = "float";
    private static final int SIZE = 8;
    private static BaseType instance;

    private FLOAT() {
        super(NAME, SIZE);
    }

    @Override
    public boolean isConvertible(BaseType baseType) {
        return false;
    }

    @Override
    public String getScopeName() {
        return null;
    }

    @Override
    public Scope getEnclosingScope() {
        return null;
    }

    @Override
    public Object getDefaultValue() {
        return 0.0f;
    }

    public static BaseType getInstance() {
        if (instance == null){
            instance = new FLOAT();
        }
        return instance;
    }
}
