package LuluCompiler.Types;

import LuluCompiler.Symbol;

public abstract class BaseType implements BasicMethods {
    private String name;
    private int size;
    protected static BaseType instance;
    private Object defaultValue = null;

    public BaseType(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return name;
    }
}
