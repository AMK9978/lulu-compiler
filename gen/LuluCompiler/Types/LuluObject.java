package LuluCompiler.Types;

import LuluCompiler.Scope;
import LuluCompiler.Symbol;
import org.objectweb.asm.Type;

import javax.naming.NamingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class LuluObject extends BaseType implements Scope {
    //The type that this class directly extends,
    //or null if it extends Object or isn't a reference type.
    private LuluObject superLuluObject;

    //The name of this LuluObject.
    private String name;

    //The symbol table containing all fields and methods.  
    //Methods in this symbol table always have a () on the
    //end of their name to distinguish them from variables.
    //eg.
    //int f;
    //is different and distinguishable from
    //int f(){
    //  return 0;
    //}
    private Map<String, Symbol> symTable = new HashMap<>();

    /**
     * Constructs a new LuluObject with the given name
     *
     * @param name The name of the class that you are representing
     */
    public LuluObject(String name) throws NamingException {
        super(name, 0);
//        if (name.equals(STRING.NAME) || name.equals(FLOAT.NAME) || name.equals(INT.NAME) || name.equals(BOOL.NAME)){
//            throw new NamingException();
//        }
        this.name = name;
    }

    /**
     * sets the superclass of this LuluObject
     *
     * @param superLuluObject the super of this LuluObject
     */
    public void setSuperLuluObject(LuluObject superLuluObject) {
        this.superLuluObject = superLuluObject;
    }

    /**
     * @return the symbol-table representation of this LuluObject's superclass
     */
    public LuluObject getSuperLuluObject() {
        return this.superLuluObject;
    }

    public int getSize(){
        int size = 0;
        for (String key : symTable.keySet()) {
            size += symTable.get(key).getType().getSize();
        }
        return size;
    }

    @Override
    public String getScopeName() {
        return name;
    }

    /**
     * This is the highest scope level, so this method returns null.
     */
    @Override
    public Scope getEnclosingScope() {
        return null;
    }

    @Override
    public Object getDefaultValue() {
        return null;
    }

    /**
     * Define a symbol in the current scope
     *
     * @param sym The symbol that you want to define.
     */
    @Override
    public void define(Symbol sym) {
        symTable.put(sym.getName(), sym);
    }

    /**
     * Since all fields of a class are initialized to 0,
     * false or null by default, this method should never be called.
     */
    @Override
    public void initialize(Symbol sym) {
        assert false;
    }

    /**
     * @param baseType the LuluObject that may be this LuluObject's ancestor
     * @return true if this LuluObject is a descendant(direct or indirect) of other,  false otherwise
     */
    public boolean isDescendantOf(BaseType baseType) {
        if (this.superLuluObject == null && baseType != this) {
            return false;
        } else if (baseType == this) {
            return true;
        } else {
            return this.superLuluObject.isDescendantOf(baseType);
        }
    }

    /**
     * Searches this LuluObject for a symbol with the given name.
     * If no such symbol exists,
     * returns getSuperLuluObject()==null? null : getSuperLuluObject().lookup(name)
     *
     * @param name The name of the symbol to search for
     * @return the symbol with symbolName equal to name.
     */
    @Override
    public Symbol lookup(String name) {
        Symbol symbol = null;
        for (LuluObject LuluObject = this; symbol == null && LuluObject != null; LuluObject = LuluObject.getSuperLuluObject()) {
            symbol = LuluObject.symTable.get(name);
        }
        return symbol;
    }

    /**
     * Searches this LuluObject for a symbol with the given name.
     * If no such symbol exists, return null.
     *
     * @param name The name of the symbol to search for.
     * @return the symbol with the given name, or null if no such symbol exists.
     */
    @Override
    public Symbol lookupLocally(String name) {
        return symTable.get(name);
    }

    /**
     * Since all field variables are intialized upon construction of an object,
     * this method returns true if this.lookup(name)!=null
     * and false otherwise.
     *
     * @param name The name of the symbol to search for.
     * @return true if this LuluObject contains the field with the given name.
     */
    @Override
    public boolean hasBeenInitialized(String name) {
        return this.lookup(name) != null;
    }

    /**
     * This method should never be called.
     *
     * @return null
     */
    @Override
    public Set<Symbol> getInitializedVariables() {
        assert false;
        return null;
    }

    @Override
    public Set<Symbol> getLocals() {
        return null;
    }

    @Override
    public int getMemoryOffset() {
        return 0;
    }

    /**
     * @return The name of this LuluObject
     */
    public String toString() {
        return name;
    }

    /**
     * اینو فعلا بیخیال شدم!
     *
     * @return an asm Type representation of this class.
     */
    public Type asAsmType() {
        if (this.name.equals("int")) {
            return Type.INT_TYPE;
        } else if (this.name.equals("bool")) {
            return Type.BOOLEAN_TYPE;
        } else if (this.name.equals("int[]")) {
            return Type.getType(int[].class);
        } else if (this.name.equals("bool[]")) {
            return Type.getType(int[].class);
        } else if (this.name.equals("float[]")) {
            return Type.getType(int[].class);
        } else if (this.name.equals("string[]")) {
            return Type.getType(int[].class);
        } else if (this.name.equals("string[]")) {
            return Type.getType(int[].class);
        } else {
            return Type.getType("L" + this.name + ";");
        }
    }

    /**
     * Can this converts to luluObject1 ?
     *
     * @param baseType The base luluObject
     * @return true if this luluObject2 can be converted to baseType
     */
    public boolean isConvertible(BaseType baseType) {
        if (baseType.getName().equals(this.name)) {
            return true;
        }
        if (baseType.getName().equals("int")) {
            return this.name.equals("bool") || this.name.equals("float") ||
                    this.name.equals("string");
        }
        if (baseType.getName().equals("bool")) {
            return this.name.equals("int");
        }

        if (!(baseType.getName().equals("float") || baseType.getName().equals("string"))) {
            return this.name.equals("int");
        }

        return isDescendantOf(baseType);
    }
}
