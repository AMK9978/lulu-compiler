package LuluCompiler.Types;

import LuluCompiler.Scope;

public interface BasicMethods {

    String getName();

    int getSize();

    boolean isConvertible(BaseType baseType);
    /**
     * @return the name of this scope.
     */
    String getScopeName();

    /**
     * Where to look next for symbols;
     * @return the scope that contains this scope.
     */
    Scope getEnclosingScope();

    Object getDefaultValue();
}
