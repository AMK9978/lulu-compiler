package LuluCompiler;// Generated from /home/amk/IdeaProjects/LULU/src/lulu.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link luluParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface luluVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link luluParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(luluParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#test}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTest(luluParser.TestContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#ft_dcl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFt_dcl(luluParser.Ft_dclContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#func_dcl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_dcl(luluParser.Func_dclContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#args}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgs(luluParser.ArgsContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#args_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgs_var(luluParser.Args_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#type_dcl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_dcl(luluParser.Type_dclContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#var_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_def(luluParser.Var_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#var_val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_val(luluParser.Var_valContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#ft_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFt_def(luluParser.Ft_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#type_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_def(luluParser.Type_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#component}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComponent(luluParser.ComponentContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#access_modifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAccess_modifier(luluParser.Access_modifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#fun_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFun_def(luluParser.Fun_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(luluParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt(luluParser.StmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#assign}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign(luluParser.AssignContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar(luluParser.VarContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#ref}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRef(luluParser.RefContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(luluParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#func_call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_call(luluParser.Func_callContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitList(luluParser.ListContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#handle_call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHandle_call(luluParser.Handle_callContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#params}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParams(luluParser.ParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#cond_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCond_stmt(luluParser.Cond_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#switch_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitch_body(luluParser.Switch_bodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#loop_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoop_stmt(luluParser.Loop_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(luluParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#const_val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConst_val(luluParser.Const_valContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#unary_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_op(luluParser.Unary_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#op1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOp1(luluParser.Op1Context ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#op2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOp2(luluParser.Op2Context ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#op3}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOp3(luluParser.Op3Context ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#op4}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOp4(luluParser.Op4Context ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#bitwise}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise(luluParser.BitwiseContext ctx);
	/**
	 * Visit a parse tree produced by {@link luluParser#logical}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical(luluParser.LogicalContext ctx);
}