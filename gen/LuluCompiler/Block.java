package LuluCompiler;

import java.util.HashMap;
import java.util.Map;
import java.util.*;

public class Block implements Scope {
    private Map<String, Symbol> locals = new HashMap<>();
    private Map<String, Symbol> initializedVariables = new HashMap<>();
    int memoryOffset = 0;
    private Scope enclosingScope;
    private String scopeName = "local";

    Block(Scope enclosingScope) {
        this.enclosingScope = enclosingScope;
    }

    @Override
    public String getScopeName() {
        return scopeName;
    }

    /**
     * Where to look next for symbols;
     */
    @Override
    public Scope getEnclosingScope() {
        return enclosingScope;
    }

    /**
     * Define a symbol in the current scope
     */
    @Override
    public void define(Symbol sym) {
        locals.put(sym.getName(), sym);
        if (sym.isPrimitive()) {
            sym.setOffset(memoryOffset);
            memoryOffset += sym.getValue_size_in_memory();
        }
    }

    @Override
    public void initialize(Symbol sym) {
        initializedVariables.put(sym.getName(), sym);
        if (!sym.isPrimitive()) {
            sym.setOffset(memoryOffset);
            memoryOffset += sym.getValue_size_in_memory();
        }
    }

    @Override
    public Symbol lookup(String name) {
        if (locals.containsKey(name)) {
            return locals.get(name);
        } else {
            return this.getEnclosingScope().lookup(name);
        }
    }

    @Override
    public Symbol lookupLocally(String name) {
        return locals.get(name);
    }

    @Override
    public boolean hasBeenInitialized(String name) {
        if (initializedVariables.containsKey(name)) {
            return true;
        } else {
            return this.getEnclosingScope().hasBeenInitialized(name);
        }
    }

    @Override
    public Set<Symbol> getInitializedVariables() {
        return new HashSet<>(this.initializedVariables.values());
    }

    @Override
    public Set<Symbol> getLocals() {
        return new HashSet<>(this.locals.values());
    }

    @Override
    public int getMemoryOffset() {
        return memoryOffset;
    }

    @Override
    public String toString() {
        return "Block{" +
                "locals=" + locals +
                ",\n initializedVariables=" + initializedVariables +
                ",\n memoryOffset=" + memoryOffset +
                ",\n enclosingScope=" + enclosingScope +
                ",\n scopeName='" + scopeName + '\'' +
                "\n}";
    }
}