package LuluCompiler;

import LuluCompiler.Types.BaseType;
import LuluCompiler.Types.LuluObject;
import LuluCompiler.Types.PrimitiveTypes.BOOL;
import LuluCompiler.Types.PrimitiveTypes.FLOAT;
import LuluCompiler.Types.PrimitiveTypes.INT;
import LuluCompiler.Types.PrimitiveTypes.STRING;

public class Symbol {
    //The name of this symbol.
    protected String name;
    //The type of this symbol.
    private BaseType type;
    //The size of symbol
    private int size = 0;
    //The value size of symbol
    private int value_size_in_memory = 0;
    private boolean isPrimitive = false;
    private Integer offset = null;
    private Object value = null;
    //A flag representing whether or not this Symbol is a field.
    private boolean isField;
    private boolean isConst = false;
    //The unique identifier of this Symbol if it is a local.
    //Used exclusively in code generation.
    private int localIdentifier = -1;
    //The unique identifier of this symbol if it is a parameter.
    //Used exclusively in code generation.
    private int parameterListIdentifier = -1;

    public boolean isPrimitive() {
        return isPrimitive;
    }

    public void setPrimitive(boolean primitive) {
        isPrimitive = primitive;
        setSize(this.getType().getSize());
        setValue_size_in_memory(getSize());
    }

    public void setType(BaseType type) {
        this.type = type;
    }

    public int getValue_size_in_memory() {
        return value_size_in_memory;
    }

    public void setValue_size_in_memory(int value_size_in_memory) {
        this.value_size_in_memory = value_size_in_memory;
    }

    public boolean isParameter() {
        return parameterListIdentifier != -1;
    }

    public void setParameterIdentifier(int parameterListIdentifier) {
        this.parameterListIdentifier = parameterListIdentifier;
    }

    public int getParameterListIdentifier() {
        return parameterListIdentifier;
    }

    public boolean isField() {
        return isField;
    }

    /**
     * @return true if this symbol is a local variable and
     * it has been assigned a unique identifier.
     */
    public boolean hasLocalIdentifier() {
        return localIdentifier != -1;
    }

    public int getLocalIdentifier() {
        assert localIdentifier >= 0;
        assert !isField;
        return localIdentifier;
    }

    public void setLocalIdentifier(int localIdentifier) {
        assert !isField;
        this.localIdentifier = localIdentifier;
    }

    public Symbol(String name, BaseType type, boolean isField, int offset, boolean isConst) {
        this.name = name;
        this.type = type;
        this.isField = isField;
        if (type.getName().equals(STRING.NAME) || type.getName().equals(FLOAT.NAME) ||
                type.getName().equals(INT.NAME) || type.getName().equals(BOOL.NAME)) {
            setPrimitive(true);
            this.setOffset(offset);
        }
        this.isConst = isConst;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(LuluObject type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public void setField(boolean field) {
        isField = field;
    }

    public void setParameterListIdentifier(int parameterListIdentifier) {
        this.parameterListIdentifier = parameterListIdentifier;
    }

    public BaseType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Symbol{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", size=" + size +
                ", value_size_in_memory=" + value_size_in_memory +
                ", isPrimitive=" + isPrimitive +
                ", offset=" + offset +
                ", value=" + value +
                ", isField=" + isField +
                ", isConst=" + isConst +
                ", localIdentifier=" + localIdentifier +
                ", parameterListIdentifier=" + parameterListIdentifier +
                '}';
    }
}
