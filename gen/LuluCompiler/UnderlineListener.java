package LuluCompiler;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;

public class UnderlineListener extends BaseErrorListener {
    @Override public void syntaxError(Recognizer<?,?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        ErrorPrinter.printFileNameAndLineNumber((Token) offendingSymbol);
        System.err.println("line " + line + ":" + charPositionInLine + " " + msg);
        ErrorPrinter.underlineError(recognizer, (Token) offendingSymbol
                //,line, charPositionInLine
        );
    }
}
