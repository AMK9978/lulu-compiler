package LuluCompiler;

import LuluCompiler.Types.BaseType;
import LuluCompiler.Types.LuluObject;
import LuluCompiler.Types.PrimitiveTypes.BOOL;
import LuluCompiler.Types.PrimitiveTypes.FLOAT;
import LuluCompiler.Types.PrimitiveTypes.INT;
import LuluCompiler.Types.PrimitiveTypes.STRING;

import javax.naming.NamingException;
import java.util.HashMap;

public class ClassNamer extends luluBaseListener {
    //The collection of classes in program
    private HashMap<String, BaseType> types;
    private HashMap<String, BaseType> declared_types = new HashMap<>();

    //The parser that generated the parse tree that this listener
    //is attached to.  Useful for reporting the line number 
    //and column number of the offending token.
    private luluParser parser;

    /**
     * [ClassNamer description]
     *
     * @param types The symbol-table collection of classes
     * @param parser        The parser that generated the parse tree
     *                      that this listener is attached to.
     */
    ClassNamer(HashMap<String, BaseType> types, luluParser parser) {
        this.types = types;
        this.parser = parser;
        initPrimitives();
    }

    /**
     * Creates a LuluObject in the symbol-table with the name derived from the
     * context.  Prints a duplicateClassError if the symbol-table already
     * contains a LuluObject with that name.
     */
    @Override
    public void enterType_def(luluParser.Type_defContext ctx) {
        LuluObject currentLuluObject;
        try {
            currentLuluObject = new LuluObject(ctx.ID(0).getText());
            if (types.put(currentLuluObject.getScopeName(), currentLuluObject) != null) {
                ErrorPrinter.printDuplicateClassError(parser, ctx.ID(0).getSymbol(), currentLuluObject.getScopeName());
            }

        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void enterType_dcl(luluParser.Type_dclContext ctx) {
        LuluObject currentLuluObject;
        try {
            currentLuluObject = new LuluObject(ctx.ID().getText());
            if (declared_types.put(currentLuluObject.getScopeName(), currentLuluObject) != null) {
                ErrorPrinter.printDuplicateClassError(parser, ctx.ID().getSymbol(), currentLuluObject.getScopeName());
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    private void initPrimitives() {
        types.put(INT.NAME, INT.getInstance());
        types.put(FLOAT.NAME, FLOAT.getInstance());
        types.put(STRING.NAME, STRING.getInstance());
        types.put(BOOL.NAME, BOOL.getInstance());
    }

    @Override
    public void exitProgram(luluParser.ProgramContext ctx) {
        for(String key : declared_types.keySet()){
            if (!types.containsKey(key)){
                ErrorPrinter.printDeclarationWithoutDefinition(declared_types.get(key), ctx.start.getLine());
            }
        }
    }
}