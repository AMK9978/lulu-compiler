package LuluCompiler;

import LuluCompiler.Types.BaseType;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.DiagnosticErrorListener;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class Main {

    /**
     * The name of the input file that is being parsed.
     */
    private static String inputFile = null;
    private static File initialFile = new File("tests/test1.txt");

    public static void main(String[] args) {
        try {
            if (args.length != 0 && args[0] != null)
                initialFile = new File(args[0]);
            InputStream is = new FileInputStream(initialFile);
            ANTLRInputStream input = new ANTLRInputStream(is);
            //A lexer object for lexing(tokenize) the file input stream.
            luluLexer lexer = new luluLexer(input);
            //A stream view of the tokenized file, built by the lexer
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            //The parser taking the token stream for parse tree construction.
            luluParser parser = new luluParser(tokens);

            //A symbol-table representation of lulu types for use in semantic analysis.
            HashMap<String, BaseType> types = new HashMap<>();

            //Since java(and miniJava) program symbols don't have global scope,
            //the symbol table cannot be implemented as a single map.
            //In order to cope with this requirement, we have a collection of scopes
            //Each of which contains its own symbol-table.  
            //This collection of scopes forms a symbol-table for the program.
            //A ParseTreeProperty<T> is a map from a particular parse tree node(context), to T.
            ParseTreeProperty<Scope> scopes = new ParseTreeProperty<>();

            //A collection of the types of the left hand sides of method call expressions.
            //ex String stackToString = (new Stack()).toString();
            //To the left of this dot has type Stack ^
            //This is a "duct tape hack" to assist with code generation.
            ParseTreeProperty<BaseType> callerTypes = new ParseTreeProperty<>();

            if (args.length > 0) {
                //Assign the first argument of the program as the inputFile name
                inputFile = args[0];
                is = new FileInputStream(inputFile);
            }
            //Remove default error listeners to ensure that error messages are not repeated.
            parser.removeErrorListeners(); // remove ConsoleErrorListener
            //Reports ambiguities or errors in the grammar that have passed Antlr's static analysis of the grammar phase.
            //http://www.antlr.org/api/Java/org/antlr/v4/runtime/DiagnosticErrorListener.html
            parser.addErrorListener(new DiagnosticErrorListener());
            parser.getInterpreter().setPredictionMode(PredictionMode.LL_EXACT_AMBIG_DETECTION);
            //Reports syntax errors upon construction of the parse tree

            //Construct the parse tree, reporting all syntax errors as stated above.
            ParseTree tree = parser.program();
            //If errors were encountered during parsing, print them and stop compiling.
            ErrorPrinter.exitOnErrors();
            System.out.println(tree.toStringTree());
            //A listener for naming the classes in the symbol table
            ClassNamer namer = new ClassNamer(types, parser);
            //Traverse the parse tree, creating LuluObject and naming them.
            ParseTreeWalker.DEFAULT.walk(namer, tree);
            //If errors were encountered during naming
            //eg. Defining two classes with the same name,
            //print the error count and stop compiling.
            ErrorPrinter.exitOnErrors();
            //A listener for building the symbol table.
            MyListener myListener = new MyListener(types, scopes, parser);
            //Traverse the parse tree, filling the symbol table.
            ParseTreeWalker.DEFAULT.walk(myListener, tree);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static String getFileName() {
        return inputFile;
    }

}

