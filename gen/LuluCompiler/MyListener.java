package LuluCompiler;

import LuluCompiler.Types.BaseType;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import java.util.HashMap;

class MyListener extends luluBaseListener {
    private luluParser parser;
    //The global symbol table, mapping parse tree nodes, to their scope.
    private ParseTreeProperty<Scope> scopes;
    private HashMap<String, BaseType> types;
    //A symbol table representation of the classes in the program that is being compiled.
    //The scope of the parse tree node that is currently being traversed.
    private Scope currentScope = null;
    //Signifies whether or not the current variable declaration is a field declaration.
    boolean isField;


    MyListener(HashMap<String, BaseType> types, ParseTreeProperty<Scope> scopes, luluParser parser) {
        this.types = types;
        this.scopes = scopes;
        this.parser = parser;
    }

    @Override
    public void enterProgram(luluParser.ProgramContext ctx) {
        System.out.println("ما اومدیم!");
    }

    @Override
    public void exitProgram(luluParser.ProgramContext ctx) {
        System.out.println("All types in program:");
        for (String type_name : types.keySet()) {
            System.out.println(type_name);
        }
    }

    @Override
    public void enterFt_dcl(luluParser.Ft_dclContext ctx) {
        enterScope(ctx);
    }

    @Override
    public void exitFt_dcl(luluParser.Ft_dclContext ctx) {
        exitScope();
    }


    @Override
    public void enterFt_def(luluParser.Ft_defContext ctx) {
        enterScope(ctx);
    }

    @Override
    public void exitFt_def(luluParser.Ft_defContext ctx) {
        exitScope();
    }

    @Override
    public void enterType_def(luluParser.Type_defContext ctx) {
        enterScope(ctx);
    }

    @Override
    public void exitType_def(luluParser.Type_defContext ctx) {
        exitScope();
    }

    @Override
    public void enterFun_def(luluParser.Fun_defContext ctx) {
        enterScope(ctx);
    }

    @Override
    public void exitFun_def(luluParser.Fun_defContext ctx) {
        exitScope();
    }

    @Override
    public void enterSwitch_body(luluParser.Switch_bodyContext ctx) {
        enterScope(ctx);
    }

    @Override
    public void exitSwitch_body(luluParser.Switch_bodyContext ctx) {
        exitScope();
    }

    @Override
    public void enterBlock(luluParser.BlockContext ctx) {
        //TODO: It should be checked that where this called? to open scope or not
        enterScope(ctx);
    }

    @Override
    public void exitBlock(luluParser.BlockContext ctx) {
        exitScope();
    }

    @Override
    public void enterVar_def(luluParser.Var_defContext ctx) {
        String typeName = ctx.type().getText();
        if (!types.containsKey(typeName))
            ErrorPrinter.printTypeNotDefinedError(ctx);
        boolean is_const = true;
        if (ctx.Const() == null)
            is_const = false;
        for (int i = 0; i < ctx.var_val().size(); i++) {
            String varName = ctx.var_val(i).ref().ID().getText();
            System.out.println(is_const + " , " + varName + " , " + typeName);
            luluParser.Var_valContext var_valContext = ctx.var_val().get(i);
            if (currentScope.lookupLocally(varName) != null) {
                ErrorPrinter.printSymbolAlreadyDefinedError(parser, ctx.var_val(i).ref().ID().getSymbol(),
                        "variable", varName, currentScope.getScopeName());
            }
            Symbol symbol = new Symbol(varName, types.get(typeName), isField, ((Block) currentScope).memoryOffset, is_const);
            currentScope.define(symbol);
            if (var_valContext.expr() == null && is_const)
                ErrorPrinter.printConstWithoutAssignment();
            else if (var_valContext.expr() != null) {
                System.out.println(var_valContext.ref().ID().getSymbol().getText() + "  - " + (var_valContext.expr().op1() == null));
                currentScope.initialize(new Symbol(varName, types.get(typeName), isField, ((Block) currentScope).memoryOffset, is_const));
            }
        }
    }

    @Override
    public void exitVar_def(luluParser.Var_defContext ctx) {
        System.out.println("In " + currentScope.getScopeName());
        for (Symbol symbol : currentScope.getInitializedVariables()){
            System.out.println(symbol);
        }
        System.out.println("End");
    }

    @Override
    public void enterExpr(luluParser.ExprContext ctx) {
    }

    @Override
    public void enterVar_val(luluParser.Var_valContext ctx) {
    }

    @Override
    public void enterRef(luluParser.RefContext ctx) {

    }

    private void saveScope(ParserRuleContext ctx, Scope s) {
        scopes.put(ctx, s);
    }

    /**
     * Associates the given parse tree node with a new explicit scope.
     * This will be used in checking if a variable was initialized before use.
     * It could also be used to extend scoping rules to allow variables to be
     * declared inside statements
     * eg. while(booleanExpression){
     * int x;
     * //more code...
     * }
     *
     * @param ctx The parse tree node (context) to associate with the generated scope.
     */
    public void enterScope(ParserRuleContext ctx) {
        currentScope = new Block(currentScope);
        System.out.println("Scope " + currentScope.getScopeName() + " started");
        saveScope(ctx, currentScope);
    }

    public void exitScope() {
        System.out.println("Scope " + currentScope.getScopeName() + " ended with:");
        System.out.println(((Block)currentScope));
        System.out.println("End scope");
        currentScope = currentScope.getEnclosingScope();
    }
}
